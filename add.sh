#!/bin/bash -e

# Assumption is that this repository is cloned into "common-primitives" directory
# which is a sibling of "d3m-primitives" directory with D3M public primitives.

D3M_VERSION="$(python3 -c 'import d3m; print(d3m.__version__)')"

for PRIMITIVE_SUFFIX in $(./list_primitives.py --suffix); do
  echo "$PRIMITIVE_SUFFIX"
  python3 -m d3m.index describe -i 4 "d3m.primitives.$PRIMITIVE_SUFFIX" > primitive.json
  pushd ../primitives
  ./add.py ../jpl-primitives/primitive.json
  popd
  if [[ -e "pipelines/$PRIMITIVE_SUFFIX" ]]; then
    PRIMITIVE_PATH="$(echo ../primitives/v$D3M_VERSION/JPL-manual/d3m.primitives.$PRIMITIVE_SUFFIX/*)"
    mkdir -p "$PRIMITIVE_PATH"
    cp -r pipelines/$PRIMITIVE_SUFFIX/* "$PRIMITIVE_PATH"
  fi
  if [[ -e "pipeline_runs/$PRIMITIVE_SUFFIX" ]]; then
    PRIMITIVE_PATH="$(echo ../primitives/v$D3M_VERSION/JPL-manual/d3m.primitives.$PRIMITIVE_SUFFIX/*)"
    mkdir -p "$PRIMITIVE_PATH"
    cp -r pipelines/$PRIMITIVE_SUFFIX/* "$PRIMITIVE_PATH"
  fi
done
