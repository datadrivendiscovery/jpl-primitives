import os
import sys
from setuptools import setup, find_packages

PACKAGE_NAME = 'jpl_primitives'
MINIMUM_PYTHON_VERSION = 3, 6


def check_python_version():
    """Exit when the Python version is too low."""
    if sys.version_info < MINIMUM_PYTHON_VERSION:
        sys.exit("Python {}.{}+ is required.".format(*MINIMUM_PYTHON_VERSION))


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    raise KeyError("'{0}' not found in '{1}'".format(key, module_path))


def read_entry_points():
    with open('entry_points.ini') as entry_points:
        return entry_points.read()


check_python_version()
version = read_package_variable('__version__')

setup(
    name=PACKAGE_NAME,
    version=version,
    description='JPL Primitives',
    author=read_package_variable('__author__'),
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
        'd3m',
        'scikit-learn',
        # 'pybind11',
        # 'smac==0.10.0',
        # 'ConfigSpace==0.4.10',
        'fastai==1.0.53.post2',
        'tpot==0.11.0',
        # 'numpy>=1.16.0',
        # 'tensorflow==2.1.0', # removing because dependency is fullfilled with d3m
        # 'tensorflow-gpu==2.1.0', # removing because dependency is fullfilled with d3m
        'keras==2.3.1',
        # 'torch>=1.1.0', # faster_rcnn torchvision dependency not yet supported, needed package upgrade in d3m core
        # 'torchvision>=0.3.0' # faster_rcnn torchvision dependency not yet supported, needed package upgrade in d3m core
    ],
    entry_points=read_entry_points(),
    url='https://gitlab.com/datadrivendiscovery/jpl-primitives',
)
