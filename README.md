# JPL Primitives

This repository contains various JPL primitives. The main areas we cover right now are Keras Wrap and Object Detection.



## Submitting

Make sure updates are merged into `master` and you have `master` branch checked out!!!

Also make sure you are working with local editable installations of d3m core and this repo and move both versions in the `__init__.py` to the most current version release.

```
./add.sh
cd ../primitives

git add v2019/JPL-manual
git commit -m "updated annoations"
git push
```

Make pull request into `primitives_repo`
