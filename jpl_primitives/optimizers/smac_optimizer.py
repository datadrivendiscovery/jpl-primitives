from collections import OrderedDict
from typing import cast, Dict, List, Union, Sequence, Optional, Tuple

import numpy as np  # type: ignore
from d3m import index
# from d3m.metrics import AccuracyMetric

from smac.scenario.scenario import Scenario
from smac.facade.smac_facade import SMAC
import d3m
import os

from sklearn.model_selection import KFold, StratifiedKFold
import typing
from d3m import container, exceptions, utils as d3m_utils
from d3m.container import DataFrame as d3m_dataframe
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams, params
from d3m.primitive_interfaces.base import CallResult, DockerContainer, MultiCallResult

from .config_space import Config_Space
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import singleton
from sklearn.metrics import accuracy_score, mean_squared_error, f1_score, mean_absolute_error, roc_auc_score
from math import sqrt
import inspect

Inputs = container.DataFrame
Outputs = container.DataFrame

SVC_class = index.get_primitive('d3m.primitives.classification.svc.SKlearn')


class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):
    max_evals = hyperparams.Bounded[int](
        default=50,
        lower=1,
        upper=10000,
        description='The number of evals to evaluate',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    run_obj = hyperparams.Enumeration[str](
        values=['quality', 'runtime'],
        default='quality',
        description='Either quality or runtime, depending on what you want to optimize.',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    deterministic = hyperparams.UniformBool(
        default=False,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        description="If true, the optimization process will be repeatable.",
    )
    runcount_limit = hyperparams.Bounded[int](
        default=600,
        lower=1,
        upper=10000,
        description='Maximum number of algorithm-calls during optimization. Default: inf.',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    # should add cutoff_time,wallclock_limit, tuner-timeout
    n_splits = hyperparams.Bounded[int](
        default=10,
        lower=2,
        upper=None,
        description='number of folds to split the data',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    loss_metric = hyperparams.Enumeration[str](
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        values=['accuracy_score', 'mean_squared_error', 'f1_macro', 'f1_score',
                'root_mean_squared_error', 'mean_absolute_error', 'roc_auc_score'],
        default='accuracy_score',
        description='Type of loss to be computed.'
    )
    primitive_class = hyperparams.Primitive[SupervisedLearnerPrimitiveBase](
        default=SVC_class,
        description='Loss we want to apply to this network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )
    stratified = hyperparams.UniformBool(
        default=False,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Do stratified folds. The folds are made by preserving the percentage of samples for each class.",
    )
    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to use as training input. If any specified column cannot be parsed, it is skipped.",
    )
    use_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to use as training target. If any specified column cannot be parsed, it is skipped.",
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not use as training inputs. Applicable only if \"use_columns\" is not provided.",
    )
    exclude_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not use as training target. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='new',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
    )
    use_semantic_types = hyperparams.UniformBool(
        default=False,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
    )
    add_index_columns = hyperparams.UniformBool(
        default=False,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )
    error_on_no_input = hyperparams.UniformBool(
        default=True,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Throw an exception if no input column is selected/provided. Defaults to true to behave like sklearn. To prevent pipelines from breaking set this to False.",
    )

# considering another primitive interface than transformer


class SMACOptimizerPrimitive(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    A Primitive that takes in the dataset and uses SMAC optimization in order to optimize the hyperparameters.
    """

    __author__ = "JPL MARVIN"
    metadata = metadata_base.PrimitiveMetadata({
        "algorithm_types": [metadata_base.PrimitiveAlgorithmType.BAYESIAN_OPTIMIZATION, ],
        "name": "smac",
        "primitive_family": metadata_base.PrimitiveFamily.SCHEMA_DISCOVERY,
        "python_path": "d3m.primitives.schema_discovery.smac.JPLPrimitives",
        "source": {'name': 'JPL-manual', 'contact': 'mailto:alice.r.yepremyan@jpl.nasa.gov',
                   'uris': ['https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
                            'https://automl.github.io/SMAC3/master/quickstart.html']},
        "version": "2019.6.7",
        "id": "35321059-2a1a-31fd-9509-5494efc751c7",
        'keywords': ['optimizers', 'smac'],
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(
                git_commit=d3m.utils.current_git_commit(os.path.dirname(__file__)))
        }],
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)

        self._random_state = np.random.RandomState(self.random_seed)
        self._scenario = None
        self._smac = None
        self._clf = None
        self.cfg = None
        self.cs = None
        self.union_var = None
        self.union_choice = None

        self._training_inputs = None
        self._target_column_indices = None
        self._training_outputs = None
        self._target_names = None
        self._training_indices = None
        self._fitted = False
        self._new_training_data = False

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._inputs = inputs
        self._outputs = outputs
        self._training_inputs, self._training_indices = self._get_columns_to_fit(
            inputs, self.hyperparams)
        self._training_outputs, self._target_names, self._target_column_indices = self._get_targets(
            outputs, self.hyperparams)
        self._fitted = False
        self._new_training_data = True

    def _create_scenario(self) -> None:
        self._scenario = Scenario({"run_obj": self.hyperparams['run_obj'],  # we optimize quality (alternatively runtime)
                                   # maximum function evaluations
                                   "runcount-limit": self.hyperparams['max_evals'],
                                   "cs": self.cs,  # configuration space
                                   "deterministic": self.hyperparams['deterministic'],
                                   "output_dir": 'smac3_outputs'
                                   })

    def _objective(self, cfg):
        """
        Creates a Primitive based on a configuration and evaluates it on the
        given dataset using cross-validation.
        Parameters:
        -----------
        cfg: Configuration (ConfigSpace.ConfigurationSpace.Configuration)
            Configuration containing the parameters.
            Configurations are indexable!
        Returns:
        --------
        A crossvalidated mean score on the loaded data-set.
        """

        cfg = {k: cfg[k] for k in cfg}

        # We translate None values:
        for item, key in cfg.items():
            if key == "None":
                cfg[item] = None

        cfg = self._translate_union_value(self.union_var, cfg)
        cfg = self._translate_union_value(self.union_choice, cfg)

        d3m_cfg = self.hyperparams['primitive_class'].metadata.query(
        )['primitive_code']['class_type_arguments']['Hyperparams'].defaults().replace(cfg)
        primitive_obj = self.hyperparams['primitive_class']

        if not inspect.isclass(primitive_obj):
            primitive_obj = primitive_obj.__class__
        clf = primitive_obj(hyperparams=d3m_cfg)

        # implement k-fold
        # try with the common primitive kfold_split.py?
        scores = []
        if self.hyperparams['stratified']:
            cv = StratifiedKFold(
                n_splits=self.hyperparams['n_splits'],
                random_state=self._random_state,
                shuffle=False
            )
        else:
            cv = KFold(
                n_splits=self.hyperparams['n_splits'],
                random_state=self._random_state,
                shuffle=False
            )

        for train_index, test_index in cv.split(self._training_inputs, self._training_outputs):
            X_train, X_test, y_train, y_test = self._training_inputs.iloc[train_index], self._training_inputs.iloc[
                test_index], self._training_outputs.iloc[train_index], self._training_outputs.iloc[test_index]
            clf.set_training_data(inputs=X_train, outputs=y_train)
            clf.fit()
            test_outputs, _, _ = self._get_targets(y_test, self.hyperparams)
            y_pred = clf.produce(inputs=X_test)

            if self.hyperparams['loss_metric'] == 'root_mean_squared_error':
                scores.append(
                    sqrt(mean_squared_error(test_outputs, y_pred.value)))
            elif self.hyperparams['loss_metric'] == 'f1_macro':
                scores.append(
                    f1_score(test_outputs, y_pred.value, average='macro'))
            else:
                scores.append(eval(self.hyperparams['loss_metric'])(
                    test_outputs, y_pred.value))
            # scores.append(AccuracyMetric().score(truth = test_outputs, predictions = y_pred.value))
        if self.hyperparams['loss_metric'] == 'accuracy_score':
            loss = 1 - np.mean(scores)
        else:
            loss = np.mean(scores)
        return loss  # Minimize!

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        if not self._new_training_data:
            return CallResult(None)
        self._new_training_data = False

        if len(self._training_indices) > 0 and len(self._target_column_indices) > 0:
            primitive_obj = self.hyperparams['primitive_class']
            config_space = Config_Space(primitive_obj)
            self.cs = config_space.get_hp_search_space()
            self.union_var = config_space.get_union_var()
            self.union_choice = config_space.get_union_choice()
            self._create_scenario()
            self._smac = SMAC(scenario=self._scenario,
                              rng=self._random_state, tae_runner=self._objective)

            incumbent = self._smac.optimize()
            cfg = {k: incumbent[k] for k in incumbent}

            # We translate None values:
            for item, key in cfg.items():
                if key == "None":
                    cfg[item] = None

            cfg = self._translate_union_value(self.union_var, cfg)
            cfg = self._translate_union_value(self.union_choice, cfg)

            self.d3m_cfg = cfg
            d3m_hyperparameters = ['use_semantic_types', 'use_inputs_columns', 'use_outputs_columns', 'error_on_no_input', 'add_index_columns',
                                   'return_result', 'exclude_inputs_columns', 'exclude_outputs_columns']
            for item in d3m_hyperparameters:
                cfg.update({item: self.hyperparams[item]})

            if not inspect.isclass(primitive_obj):
                primitive_obj = primitive_obj.__class__

            self._clf = primitive_obj(hyperparams=cfg)
            self._clf.set_training_data(
                inputs=self._inputs, outputs=self._outputs)
            self._clf.fit()

            self._fitted = True
        else:
            if self.hyperparams['error_on_no_input']:
                raise RuntimeError("No input columns were selected")
            self.logger.warn("No input columns were selected")

        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        sklearn_input = inputs
        output = self._clf.produce(inputs=sklearn_input)
        return CallResult(output.value)

    @singleton
    def produce_optimal_hyperparameters(self, *, timeout: float = None, iterations: int = None) -> CallResult[d3m_dataframe]:
        output = d3m_dataframe([self.d3m_cfg], columns=self.d3m_cfg.keys())
        for i in range(len(output.columns)):
            output.metadata = output.metadata.update_column(
                i, {"name": output.columns[i]})
        return CallResult(output)

    def _translate_union_value(self, union_list, cfg):
        # We translate Union values:
        for item in union_list:
            if item in cfg:
                value = cfg[item]
                cfg[item] = cfg[value]
                cfg.pop(value, None)  # Remove extra union choices from config
            else:
                continue
        return cfg

    @classmethod
    def _get_columns_to_fit(cls, inputs: Inputs, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return inputs, list(range(len(inputs.columns)))

        inputs_metadata = inputs.metadata

        def can_produce_column(column_index: int) -> bool:
            return cls._can_produce_column(inputs_metadata, column_index, hyperparams)

        columns_to_produce, columns_not_to_produce = base_utils.get_columns_to_use(inputs_metadata,
                                                                                   use_columns=hyperparams[
                                                                                       'use_inputs_columns'],
                                                                                   exclude_columns=hyperparams[
                                                                                       'exclude_inputs_columns'],
                                                                                   can_use_column=can_produce_column)
        return inputs.iloc[:, columns_to_produce], columns_to_produce
        # return columns_to_produce

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int,
                            hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query(
            (metadata_base.ALL_ELEMENTS, column_index))

        accepted_structural_types = (int, float, np.integer, np.float64)
        accepted_semantic_types = set()
        accepted_semantic_types.add(
            "https://metadata.datadrivendiscovery.org/types/Attribute")
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        semantic_types = set(column_metadata.get('semantic_types', []))

        if len(semantic_types) == 0:
            cls.logger.warning("No semantic types found in column metadata")
            return False
        # Making sure all accepted_semantic_types are available in semantic_types
        if len(accepted_semantic_types - semantic_types) == 0:
            return True

        return False

    @classmethod
    def _get_targets(cls, data: d3m_dataframe, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return data, list(data.columns), list(range(len(data.columns)))

        metadata = data.metadata

        def can_produce_column(column_index: int) -> bool:
            accepted_semantic_types = set()
            accepted_semantic_types.add(
                "https://metadata.datadrivendiscovery.org/types/TrueTarget")
            column_metadata = metadata.query(
                (metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = set(column_metadata.get('semantic_types', []))
            if len(semantic_types) == 0:
                cls.logger.warning(
                    "No semantic types found in column metadata")
                return False
            # Making sure all accepted_semantic_types are available in semantic_types
            if len(accepted_semantic_types - semantic_types) == 0:
                return True
            return False

        target_column_indices, target_columns_not_to_produce = base_utils.get_columns_to_use(metadata,
                                                                                             use_columns=hyperparams[
                                                                                                 'use_outputs_columns'],
                                                                                             exclude_columns=hyperparams[
                                                                                                 'exclude_outputs_columns'],
                                                                                             can_use_column=can_produce_column)
        targets = []
        if target_column_indices:
            targets = data.select_columns(target_column_indices)
        target_column_names = []
        for idx in target_column_indices:
            target_column_names.append(data.columns[idx])
        return targets, target_column_names, target_column_indices

    def get_params(self) -> None:
        """
        A noop.
        """

        return None

    def set_params(self) -> None:
        """
        A noop.
        """

        return None
