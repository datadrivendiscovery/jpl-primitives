from tensorflow.keras import metrics as keras_metrics
from tensorflow.keras.layers import Layer as KerasBaseLayer
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import PrimitiveBase, NeuralNetworkObjectMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m import utils
from typing import Dict, Callable
from d3m.container import DataFrame as d3m_dataframe
import os
from ..base_classes import NeuralNetworkObjectBase


class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):
    pass


Inputs = d3m_dataframe
Outputs = d3m_dataframe
Module = KerasBaseLayer


class CategoricalAccuracy(NeuralNetworkObjectBase[Inputs, Outputs, Params, Hyperparams], NeuralNetworkObjectMixin[Inputs, Outputs, Params, Hyperparams, Module]):
    __keras_doc__ = keras_metrics.categorical_accuracy.__doc__ if keras_metrics.categorical_accuracy.__doc__ is not None else 'Keras documentation not available.'
    _orig = NeuralNetworkObjectBase.__doc__
    _general, _superclass = _orig.split("Pure Keras Documentation:\n\n")
    __doc__ = _general + "Pure Keras Documentation:\n\n" + \
        __keras_doc__ + "\n\n" + _superclass

    __author__ = "Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>"
    metadata = metadata_base.PrimitiveMetadata({
        'id': '393ac0d9-8d4d-4b2a-85dc-35815e8a6695',
        'version': '1.2.0',
        'name': 'categorical_accuracy',
        'keywords': ['neural network', 'deep learning'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/keras_wrap/losses/categorical_accuracy.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.loss_function.categorical_accuracy.KerasWrap',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': metadata_base.PrimitiveFamily.LOSS_FUNCTION,
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)
        self._hyperparams = hyperparams

    def get_neural_network_object(self) -> Callable:
        return(keras_metrics.categorical_accuracy)
