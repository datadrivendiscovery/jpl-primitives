import importlib
import os
import typing
from collections import OrderedDict
from typing import List, Dict, Tuple, Optional

import d3m
import numpy as np
import tensorflow as tf
from d3m.base import utils as base_utils
from d3m.container import DataFrame as d3m_dataframe
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.primitive_interfaces.base import ContinueFitMixin
from d3m.primitive_interfaces.base import NeuralNetworkModuleMixin, NeuralNetworkObjectMixin
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from sklearn.preprocessing import OneHotEncoder
from tensorflow.compat.v1.keras.models import Model as KerasModel
from tensorflow.keras.callbacks import History
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Layer as KerasBaseLayer

from jpl_primitives import utils
from .layers.null import Null
from .losses.mean_squared_error import MeanSquaredError, Hyperparams as MeanSquaredErrorHypers


class Hyperparams(hyperparams.Hyperparams):

    network_last_layer = hyperparams.Union(
        OrderedDict({
            "network_last_layer": hyperparams.Primitive[NeuralNetworkModuleMixin](
                default=Null,
                description='Composition of the neural network. Need to all implement NNLayerMixin',
                semantic_types=[
                    'https://metadata.datadrivendiscovery.org/types/TuningParameter']
            ),
            "none": hyperparams.Hyperparameter[None](
                default=None,
            )
        }),
        default='none',
        description='next layer in the neural network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    loss = hyperparams.Primitive[NeuralNetworkObjectMixin](
        default=MeanSquaredError(
            hyperparams=MeanSquaredErrorHypers.defaults()),
        description='Loss we want to apply to this network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )

    optimizer = hyperparams.Choice(
        choices={
            'SGD': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'lr': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'momentum': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'decay': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'nesterov': hyperparams.Hyperparameter[bool](
                        default=False,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RMSprop': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'lr': hyperparams.Hyperparameter[float](
                        default=0.001,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'rho': hyperparams.Hyperparameter[float](
                        default=0.9,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'epsilon': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'decay': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Adagrad': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'lr': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'epsilon': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'decay': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Adadelta': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'lr': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'rho': hyperparams.Hyperparameter[float](
                        default=0.95,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'epsilon': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'decay': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Adam': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'lr': hyperparams.Hyperparameter[float](
                        default=0.001,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'beta_1': hyperparams.Hyperparameter[float](
                        default=0.9,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'beta_2': hyperparams.Hyperparameter[float](
                        default=0.999,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'epsilon': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'decay': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'amsgrad': hyperparams.Hyperparameter[bool](
                        default=False,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Adamax': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'lr': hyperparams.Hyperparameter[float](
                        default=0.002,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'beta_1': hyperparams.Hyperparameter[float](
                        default=0.9,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'beta_2': hyperparams.Hyperparameter[float](
                        default=0.999,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'epsilon': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'decay': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Nadam': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'lr': hyperparams.Hyperparameter[float](
                        default=0.002,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'beta_1': hyperparams.Hyperparameter[float](
                        default=0.9,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'beta_2': hyperparams.Hyperparameter[float](
                        default=0.999,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'epsilon': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'schedule_decay': hyperparams.Hyperparameter[float](
                        default=0.004,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            )
        },
        default="SGD",
        description='Optimizer for neural network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    batch_size = hyperparams.UniformInt(
        default=20,
        lower=1,
        upper=99,
        description="Batch size.",
        semantic_types=['http://schema.org/Integer',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    model_type = hyperparams.Enumeration(
        default="regression",
        values=["regression", "classification"],
        description="Controls whether or not to adjust the outputs into a one hot encoding matrix or keep as original input values",
        semantic_types=['http://schema.org/String',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    use_gpu = hyperparams.UniformBool(
        default=True,
        description="Whether or not to use the gpu for computation",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )

    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    use_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )

    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        # Default value depends on the nature of the primitive.
        default='append',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should resulting columns be appended, should they replace original columns, or should only resulting columns be returned?",
    )

    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )


class Params(params.Params):
    _hyperparameters: Optional[Hyperparams]
    _fitted: Optional[bool]
    _random_seed: Optional[int]
    _model: Optional[KerasModel]
    _history: Optional[History]
    _secondary_history: Optional[History]
    _one_hot_encoder: Optional[OneHotEncoder]


Inputs = d3m_dataframe
Outputs = d3m_dataframe
Module = KerasBaseLayer


class Model(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams],
            ContinueFitMixin[Inputs, Outputs, Params, Hyperparams]):
    """
    The main learner primitive that is responsible for assembling the custom built neural network as part of Keras Wrap.
    In order to use this primitive, you should assemble your architecture via your pipeline and pass the primitive reference
    to the last layer as your 'network_last_layer'. All Keras Wrap layers inherit from NeuralNetworkModuleMixin and follow the
    naming convention 'd3m.primitives.layer.<layer_name>.KerasWrap'. You should also specify the proper loss function and any
    accompanying metrics that should be tracked during the training process as well. These primtives that are support as port of
    Keras wrap inherit from NeuralNetworkObjectMixin and follow the form 'd3m.primitives.loss_function.<loss_name>.KerasWrap'.

    This primitive will infer the size and number of channels of the input images from the data (in w x h x c format), however, 
    every image has to be square as well as all images have to be scaled to the same size.
    """
    __author__ = "Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>"
    metadata = metadata_base.PrimitiveMetadata({
        'id': 'f8b81d1a-3e22-4edf-aa99-15bcbe827954',
        'version': '1.2.0',
        'name': 'model',
        'keywords': ['neural network', 'deep learning'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/keras_wrap/model.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(
                git_commit=d3m.utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.learner.model.KerasWrap',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': metadata_base.PrimitiveFamily.LEARNER,
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)

        tf.compat.v1.enable_eager_execution()
        self.hyperparams = hyperparams
        self.random_seed = random_seed
        self._history = History()
        # Secondary history for us to manage continue_fit and append of callback data
        self._secondary_history = History()
        self.fitted = False
        self._model = None
        self.one_hot_encoder = None

    """
    Build NNModel Mixin

    """

    def _build_model(self, *, size=int, channels=int) -> None:

        # This is the last Primitive Layer before getting into network
        last_layer = self.hyperparams["network_last_layer"]

        inp = Input(shape=(size, size, channels))
        x = last_layer.get_neural_network_module(input_module=inp)

        self._model = KerasModel(inputs=inp, outputs=x, name='sdf')

        # Removing metrics for now until we are able to expose multiple values via loss mixin
        # metric = [self.hyperparams["metric"].get_neural_network_object(
        # )] if self.hyperparams["metric"] is not None else []
        metric = []
        # Instantiating appropriate Keras optimizer class with specified hyperparameters from "Choice"
        optim = self.hyperparams["optimizer"]["choice"]
        KerasOptimizer = getattr(
            importlib.import_module("tensorflow.keras.optimizers"), optim)
        _hypers = self.hyperparams["optimizer"].copy()
        del _hypers["choice"]
        optim_instance = KerasOptimizer(**_hypers)

        if self.hyperparams["use_gpu"]:
            os.environ["CUDA_VISIBLE_DEVICES"] = "0"
        else:
            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

        self._model.compile(
            optimizer=optim_instance, loss=self.hyperparams["loss"].get_neural_network_object(), metrics=metric)

        return

    """
    SupervisedLearnerPrimitiveBase methods, see: https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/primitive_interfaces/base.py


    """

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:

        selected_inputs, columns_to_use = self._select_inputs_columns(inputs)

        # The inputs at this point should be a single column with 3D arrays per row
        # We reshape to get a (nrow, h, w, c) ndarray for input
        inp = np.array(selected_inputs[selected_inputs.columns[0]].tolist())
        inp = tf.convert_to_tensor(inp, dtype=tf.float32)

        predicted_output = self._model(inp)
        predicted_output = np.array(predicted_output)
        if self.hyperparams["model_type"] == "classification":
            output_columns = predicted_output.argmax(axis=-1)
            _outputs = np.zeros(
                [len(output_columns), self.one_hot_encoder.categories_[0].shape[0]])
            _outputs[np.arange(_outputs.shape[0]), output_columns] = 1
            predicted_output = self.one_hot_encoder.inverse_transform(
                _outputs).flatten()

        output_columns = [self._wrap_predictions(predicted_output)]

        outputs = base_utils.combine_columns(inputs, columns_to_use, output_columns,
                                             return_result=self.hyperparams['return_result'],
                                             add_index_columns=self.hyperparams['add_index_columns'])

        return CallResult(outputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        self._training_outputs = outputs
        return

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        inputs, _ = self._select_inputs_columns(self._training_inputs)
        outputs, _ = self._select_outputs_columns(self._training_outputs)

        self._store_columns_metadata_and_names(inputs, outputs)

        # The inputs at this point should be a single column with 3D arrays per row
        # We reshape to get a (nrow, h, w, c) ndarray for input
        inp = np.array(inputs[inputs.columns[0]].tolist())
        self._inp_size = inp[0].shape[0]
        self._inp_channels = inp[0].shape[-1]
        # If we have categorical data, we need to one hot encode our category labels
        if self.hyperparams["model_type"] == "classification":
            self._init_one_hot_encoder()
            outp = self.one_hot_encoder.transform(outputs).astype('int32')
        else:
            outp = outputs

        if iterations is not None:
            epochs = iterations
        else:
            epochs = 2

        total_inputs = inp.shape[0]
        inp = tf.convert_to_tensor(inp, dtype=tf.float32)
        outp = tf.convert_to_tensor(outp, dtype=tf.float32)

        self._build_model(size=self._inp_size, channels=self._inp_channels)
        spe = int(total_inputs/self.hyperparams["batch_size"])
        self._model.fit(inp, outp, epochs=epochs,
                        steps_per_epoch=spe, callbacks=[self._history])

        self.fitted = True

        return CallResult(None)

    def get_params(self) -> Params:
        return Params(
            _hyperparameters=self.hyperparams, _fitted=self.fitted, _random_seed=self.random_seed, _model=self._model,
            _history=self._history, _secondary_history=self._secondary_history, _one_hot_encoder=self.one_hot_encoder
        )

    def set_params(self, *, params: Params) -> None:
        if params is None:
            return

        self.hyperparams = params['_hyperparameters']
        self.fitted = params['_fitted']
        self.random_seed = params['_random_seed']
        self._model = params['_model']
        self._history = params['_history']
        self._secondary_history = params['_secondary_history']
        self.one_hot_encoder = params['_one_hot_encoder']

    def _init_one_hot_encoder(self):
        self.one_hot_encoder = OneHotEncoder(sparse=False)
        unique_values = None
        for i in range(len(self._training_outputs.columns)):
            column_semantic_types = self._training_outputs.metadata.query(
                (metadata_base.ALL_ELEMENTS, i))['semantic_types']
            if 'https://metadata.datadrivendiscovery.org/types/TrueTarget' in column_semantic_types:
                unique_values = self._training_outputs.metadata.query(
                    (metadata_base.ALL_ELEMENTS, i))['all_distinct_values']
                break
        unique_values = np.array(unique_values).reshape(-1, 1)
        self.one_hot_encoder.fit(unique_values)

    """
    ContinueFitMixin methods, see: https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/primitive_interfaces/base.py


    """

    def continue_fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:

        # if model has not fitted, the model has not been initialized, so we call fit.
        if not self.fitted:
            self.fit(timeout=timeout, iterations=iterations)
            self.fitted = True
            return CallResult(None)

        inputs, _ = self._select_inputs_columns(self._training_inputs)
        outputs, _ = self._select_outputs_columns(self._training_outputs)

        # The inputs at this point should be a single column with 3D arrays per row
        # We reshape to get a (nrow, h, w, c) ndarray for input
        inp = np.array(inputs[inputs.columns[0]].tolist())

        # If we have categorical data, we need to one hot encode our category labels
        if self.hyperparams["model_type"] == "classification":
            # outp = to_categorical(outputs)
            outp = self.one_hot_encoder.transform(outputs).astype('int32')
        else:
            outp = outputs

        if iterations is not None:
            epochs = iterations
        else:
            epochs = 2

        total_inputs = inp.shape[0]
        spe = int(total_inputs/self.hyperparams["batch_size"])

        inp = tf.convert_to_tensor(inp, dtype=tf.float32)
        outp = tf.convert_to_tensor(outp, dtype=tf.float32)

        with utils.TensorflowControlledRandomness(seed=self.random_seed):

            self._model.fit(inp, outp, epochs=epochs, steps_per_epoch=spe, callbacks=[
                            self._secondary_history])
            # We have to manage callback history state because Keras does not allow us to
            # do so within the package and we want a running total from the continue_fit method
            for key, val in self._secondary_history.history.items():
                self._history.history[key] = self._history.history[key] + \
                    self._secondary_history.history[key]

        return CallResult(None)

    """
    Helper methods (Will be moved to SupervisedLearner Base class eventually
    
    """

    @classmethod
    def _can_use_inputs_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = inputs_metadata.query(
            (metadata_base.ALL_ELEMENTS, column_index))

        accepted_structural_types = (np.ndarray)
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        return 'https://metadata.datadrivendiscovery.org/types/Attribute' in column_metadata.get('semantic_types', [])

    @classmethod
    def _get_inputs_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_inputs_column(inputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(inputs_metadata,
                                                                      hyperparams['use_inputs_columns'],
                                                                      hyperparams['exclude_inputs_columns'],
                                                                      can_use_column)

        if not columns_to_use:
            raise ValueError("No inputs columns.")

        if hyperparams['use_inputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified inputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    @classmethod
    def _can_use_outputs_column(cls, outputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = outputs_metadata.query(
            (metadata_base.ALL_ELEMENTS, column_index))

        return 'https://metadata.datadrivendiscovery.org/types/SuggestedTarget' in column_metadata.get('semantic_types',
                                                                                                       [])

    @classmethod
    def _get_outputs_columns(cls, outputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_outputs_column(outputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(outputs_metadata,
                                                                      hyperparams['use_outputs_columns'],
                                                                      hyperparams['exclude_outputs_columns'],
                                                                      can_use_column)

        if not columns_to_use:
            raise ValueError("No outputs columns.")

        if hyperparams['use_outputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified outputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    def _select_inputs_columns(self, inputs: Inputs) -> Tuple[Inputs, List[int]]:
        columns_to_use = self._get_inputs_columns(
            inputs.metadata, self.hyperparams)

        return utils.select_columns(inputs, columns_to_use, source=self), columns_to_use

    def _select_outputs_columns(self, outputs: Outputs) -> Tuple[Outputs, List[int]]:
        columns_to_use = self._get_outputs_columns(
            outputs.metadata, self.hyperparams)

        return utils.select_columns(outputs, columns_to_use, source=self), columns_to_use

    @classmethod
    def _add_target_semantic_types(cls, metadata: metadata_base.DataMetadata,
                                   source: typing.Any, target_names: List = None, ) -> metadata_base.DataMetadata:
        for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/Target',
                                                  source=source)
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                                                  source=source)
            if target_names:
                metadata = metadata.update((metadata_base.ALL_ELEMENTS, column_index), {
                    'name': target_names[column_index],
                }, source=source)
        return metadata

    def _wrap_predictions(self, predictions: np.ndarray) -> Outputs:
        outputs = d3m_dataframe(predictions, generate_metadata=False)
        outputs.metadata = self._update_predictions_metadata(
            outputs, self._target_columns_metadata)
        outputs.columns = self._target_columns_names
        return outputs

    @classmethod
    def _update_predictions_metadata(cls, outputs: Optional[Outputs],
                                     target_columns_metadata: List[OrderedDict]) -> metadata_base.DataMetadata:
        outputs_metadata = metadata_base.DataMetadata()
        if outputs is not None:
            outputs_metadata = outputs_metadata.generate(outputs)

        for column_index, column_metadata in enumerate(target_columns_metadata):
            outputs_metadata = outputs_metadata.update_column(
                column_index, column_metadata)

        return outputs_metadata

    def _store_columns_metadata_and_names(self, inputs: Inputs, outputs: Outputs) -> None:
        self._attribute_columns_names = list(inputs.columns)
        self._target_columns_metadata = self._get_target_columns_metadata(
            outputs.metadata)
        self._target_columns_names = list(outputs.columns)

    @classmethod
    def _get_target_columns_metadata(cls, outputs_metadata: metadata_base.DataMetadata) -> List[OrderedDict]:
        outputs_length = outputs_metadata.query((metadata_base.ALL_ELEMENTS,))[
            'dimension']['length']

        target_columns_metadata: List[OrderedDict] = []
        for column_index in range(outputs_length):
            column_metadata = OrderedDict(
                outputs_metadata.query_column(column_index))

            # Update semantic types and prepare it for predicted targets.
            semantic_types = list(column_metadata.get('semantic_types', []))
            if 'https://metadata.datadrivendiscovery.org/types/PredictedTarget' not in semantic_types:
                semantic_types.append(
                    'https://metadata.datadrivendiscovery.org/types/PredictedTarget')
            semantic_types = [semantic_type for semantic_type in semantic_types if
                              semantic_type != 'https://metadata.datadrivendiscovery.org/types/TrueTarget']
            column_metadata['semantic_types'] = semantic_types

            target_columns_metadata.append(column_metadata)

        return target_columns_metadata
