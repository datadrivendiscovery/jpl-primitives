from tensorflow.keras.layers import Layer as KerasBaseLayer
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import PrimitiveBase, NeuralNetworkModuleMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m import utils
from typing import Dict, Union, Tuple
from d3m.primitive_interfaces.base import *


Module = KerasBaseLayer


class LayerBase(PrimitiveBase[Inputs, Outputs, Params, Hyperparams], NeuralNetworkModuleMixin[Inputs, Outputs, Params, Hyperparams, Module]):
    """
    A neural network layer that has been wrapped from Keras. You can assemble these layers togetherto form any architecture
    nerual network. To assemble, every layer has a hyperparameter 'previous_layer'. This hyperparameter takes in another Keras wrapped
    layer primitive and you are allowed to chain your neural network together. This is chained until the very first layer you
    want to serve as your initial input layer.

    Pure Keras Documentation:\n\n
    """

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        return CallResult(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        return None

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        return CallResult(None)

    def get_params(self) -> Params:
        pass

    def set_params(self, *, params: Params) -> None:
        pass


class NeuralNetworkObjectBase(PrimitiveBase[Inputs, Outputs, Params, Hyperparams], NeuralNetworkObjectMixin[Inputs, Outputs, Params, Hyperparams, Module]):
    """
    A neural network object that represents a reference to a function. These objects are used to specify
    a particular type of loss or metric for the 'Learner' primitive, which constructs it's architecture from scratch.

    Pure Keras Documentation:\n\n
    """

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        return CallResult(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        return None

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        return CallResult(None)

    def get_params(self) -> Params:
        pass

    def set_params(self, *, params: Params) -> None:
        pass
