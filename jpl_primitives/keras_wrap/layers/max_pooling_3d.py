from tensorflow.keras import layers as k_layers
from tensorflow.keras.layers import Layer as KerasBaseLayer
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import PrimitiveBase, NeuralNetworkModuleMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m import utils
from typing import Dict
from d3m.container import DataFrame as d3m_dataframe
import os
from collections import OrderedDict
from .null import Null
from ..base_classes import LayerBase


class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):

    previous_layer = hyperparams.Union(
        OrderedDict({
            "previous_layer": hyperparams.Primitive[NeuralNetworkModuleMixin](
                default=Null,
                description='Composition of the neural network. Need to all implement NNLayerMixin',
                semantic_types=[
                    'https://metadata.datadrivendiscovery.org/types/TuningParameter']
            ),
            "none": hyperparams.Hyperparameter[None](
                default=None,
            )
        }),
        default='none',
        description='next layer in the neural network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    pool_size = hyperparams.UniformInt(
        default=2,
        lower=1,
        upper=20,
        description="An integer or tuple/list of a single integer, specifying the length of the 1D convolution window.",
        semantic_types=['http://schema.org/Integer',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    strides = hyperparams.UniformInt(
        default=1,
        lower=1,
        upper=20,
        description="An integer or tuple/list of a single integer, specifying the stride length of the convolution. Specifying any stride value != 1 is incompatible with specifying any dilation_rate value != 1.",
        semantic_types=['http://schema.org/Integer',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    padding = hyperparams.Enumeration(
        default="valid",
        values=["valid", "causal", "same"],
        description="One of 'valid', 'causal' or 'same' (case-insensitive).  'valid' means 'no padding'.  'same' results in padding the input such that the output has the same length as the original input.  'causal' results in causal (dilated) convolutions, e.g. output[t] does not depend on input[t + 1:]. A zero padding is used such that the output has the same length as the original input. Useful when modeling temporal data where the model should not violate the temporal order. See WaveNet: A Generative Model for Raw Audio, section 2.1.",
        semantic_types=['http://schema.org/String',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    data_format = hyperparams.Enumeration(
        default="channels_last",
        values=["channels_last", "channels_first"],
        description="A string, one of 'channels_last' (default) or 'channels_first'. The ordering of the dimensions in the inputs.  'channels_last' corresponds to inputs with shape  (batch, steps, channels) (default format for temporal data in Keras) while 'channels_first' corresponds to inputs with shape (batch, channels, steps).",
        semantic_types=['http://schema.org/String',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )


Inputs = d3m_dataframe
Outputs = d3m_dataframe
Module = KerasBaseLayer


class MaxPooling3D(LayerBase[Inputs, Outputs, Params, Hyperparams], NeuralNetworkModuleMixin[Inputs, Outputs, Params, Hyperparams, Module]):
    _orig = LayerBase.__doc__
    _general, _superclass = _orig.split("Pure Keras Documentation:\n\n")
    __doc__ = _general + "Pure Keras Documentation:\n\n" + \
        k_layers.MaxPooling3D.__doc__ + "\n\n" + _superclass

    __author__ = "Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>"
    metadata = metadata_base.PrimitiveMetadata({
        'id': 'd18c07de-d7a8-4ed8-af5b-2b075ee6572a',
        'version': '1.2.0',
        'name': 'max_pooling_3d',
        'keywords': ['neural network', 'deep learning'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/keras_wrap/layers/max_pooling_3d.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.layer.max_pooling_3d.KerasWrap',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': metadata_base.PrimitiveFamily.LAYER,
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)
        self._hyperparams = hyperparams
        self._modules = {}

    def _adjust_hypers(self):

        hypers = self._hyperparams.copy()

        # Layer
        del hypers["previous_layer"]

        return hypers

    def get_neural_network_module(self, *, input_module: Module) -> Module:

        if input_module not in self._modules:

            previous_layer = self._hyperparams["previous_layer"]
            hypers = self._adjust_hypers()

            if previous_layer is not None:
                previous_layer_module = previous_layer.get_neural_network_module(
                    input_module=input_module)
            else:
                previous_layer_module = input_module
            current_layer_module = k_layers.MaxPooling3D(
                **hypers)(previous_layer_module)

            self._modules[input_module] = current_layer_module

        return self._modules[input_module]
