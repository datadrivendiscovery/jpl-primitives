from tensorflow.keras import layers as k_layers
from tensorflow.keras.layers import Layer as KerasBaseLayer
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import PrimitiveBase, NeuralNetworkModuleMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m import utils
from typing import Dict, Union
from d3m.container import DataFrame as d3m_dataframe
import os
from collections import OrderedDict
from .null import Null
import importlib
from ..base_classes import LayerBase


class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):

    previous_layer = hyperparams.Union(
        OrderedDict({
            "previous_layer": hyperparams.Primitive[NeuralNetworkModuleMixin](
                default=Null,
                description='Composition of the neural network. Need to all implement NNLayerMixin',
                semantic_types=[
                    'https://metadata.datadrivendiscovery.org/types/TuningParameter']
            ),
            "none": hyperparams.Hyperparameter[None](
                default=None,
            )
        }),
        default='none',
        description='next layer in the neural network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    filters = hyperparams.UniformInt(
        default=10,
        lower=1,
        upper=999999999999,
        description='Integer, the dimensionality of the output space (i.e. the number of output filters in the convolution).',
        semantic_types=['http://schema.org/Integer',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    kernel_size = hyperparams.UniformInt(
        default=2,
        lower=1,
        upper=20,
        description="An integer or tuple/list of a single integer, specifying the length of the 1D convolution window.",
        semantic_types=['http://schema.org/Integer',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    strides = hyperparams.UniformInt(
        default=1,
        lower=1,
        upper=20,
        description="An integer or tuple/list of a single integer, specifying the stride length of the convolution. Specifying any stride value != 1 is incompatible with specifying any dilation_rate value != 1.",
        semantic_types=['http://schema.org/Integer',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    padding = hyperparams.Enumeration(
        default="valid",
        values=["valid", "causal", "same"],
        description="One of 'valid', 'causal' or 'same' (case-insensitive).  'valid' means 'no padding'.  'same' results in padding the input such that the output has the same length as the original input.  'causal' results in causal (dilated) convolutions, e.g. output[t] does not depend on input[t + 1:]. A zero padding is used such that the output has the same length as the original input. Useful when modeling temporal data where the model should not violate the temporal order. See WaveNet: A Generative Model for Raw Audio, section 2.1.",
        semantic_types=['http://schema.org/String',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    data_format = hyperparams.Enumeration(
        default="channels_last",
        values=["channels_last", "channels_first"],
        description="A string, one of 'channels_last' (default) or 'channels_first'. The ordering of the dimensions in the inputs.  'channels_last' corresponds to inputs with shape  (batch, steps, channels) (default format for temporal data in Keras) while 'channels_first' corresponds to inputs with shape (batch, channels, steps).",
        semantic_types=['http://schema.org/String',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    dilation_rate = hyperparams.UniformInt(
        default=1,
        lower=1,
        upper=20,
        description="an integer or tuple/list of a single integer, specifying the dilation rate to use for dilated convolution. Currently, specifying any dilation_rate value != 1 is incompatible with specifying any strides value != 1.",
        semantic_types=['http://schema.org/String',
                        'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    activation = hyperparams.Enumeration(
        default="linear",
        values=["softmax", "elu", "selu", "softplus", "softsign", "relu", "tanh", "hard_sigmoid", "exponential",
                "linear"],
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    use_bias = hyperparams.UniformBool(
        default=True,
        description="whether or not to use bias",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    kernel_initializer = hyperparams.Choice(
        choices={
            'Zeros': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Ones': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Constant': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'stddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomUniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'minval': hyperparams.Hyperparameter[float](
                        default=-0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'maxval': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'TruncatedNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'steddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'VarianceScaling': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'scale': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'mode': hyperparams.Enumeration(
                        default="fan_in",
                        values=["fan_in", "fan_out", "fan_avg"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'distribution': hyperparams.Enumeration(
                        default="normal",
                        values=["normal", "uniform"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Orthogonal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Identity': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'lecun_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'lecun_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            )
        },
        default="glorot_uniform",
        description='kernel initializer for the network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    bias_initializer = hyperparams.Choice(
        choices={
            'Zeros': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Ones': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Constant': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'stddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomUniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'minval': hyperparams.Hyperparameter[float](
                        default=-0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'maxval': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'TruncatedNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'steddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'VarianceScaling': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'scale': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'mode': hyperparams.Enumeration(
                        default="fan_in",
                        values=["fan_in", "fan_out", "fan_avg"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'distribution': hyperparams.Enumeration(
                        default="normal",
                        values=["normal", "uniform"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Orthogonal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Identity': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'lecun_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'lecun_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            )
        },
        default="glorot_normal",
        description='bias initializer for the network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    kernel_regularizer = hyperparams.Choice(
        choices={
            'l1': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l1_l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l1': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'l2': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="kernel regularizer for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    bias_regularizer = hyperparams.Choice(
        choices={
            'l1': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l1_l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l1': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'l2': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="kernel regularizer for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    activity_regularizer = hyperparams.Choice(
        choices={
            'l1': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l1_l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l1': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'l2': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="kernel regularizer for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    kernel_constraint = hyperparams.Choice(
        choices={
            'MaxNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'max_value': hyperparams.Hyperparameter[int](
                        default=2,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'NonNeg': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'UnitNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'MinMaxNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'min_value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'max_value': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'rate': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="kernel constraint for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    bias_constraint = hyperparams.Choice(
        choices={
            'MaxNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'max_value': hyperparams.Hyperparameter[int](
                        default=2,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'NonNeg': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'UnitNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'MinMaxNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'min_value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'max_value': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'rate': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="bias constraint for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )


Inputs = d3m_dataframe
Outputs = d3m_dataframe
Module = KerasBaseLayer


class Conv1D(LayerBase[Inputs, Outputs, Params, Hyperparams], NeuralNetworkModuleMixin[Inputs, Outputs, Params, Hyperparams, Module]):
    _orig = LayerBase.__doc__
    _general, _superclass = _orig.split("Pure Keras Documentation:\n\n")
    __doc__ = _general + "Pure Keras Documentation:\n\n" + \
        k_layers.Conv1D.__doc__ + "\n\n" + _superclass

    __author__ = "Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>"
    metadata = metadata_base.PrimitiveMetadata({
        'id': 'c9083eac-ad21-464f-81df-a32a6267b85a',
        'version': '1.2.0',
        'name': 'convolution_1d',
        'keywords': ['neural network', 'deep learning'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/keras_wrap/layers/convolution_1d.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.layer.convolution_1d.KerasWrap',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': metadata_base.PrimitiveFamily.LAYER,
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)
        self._hyperparams = hyperparams
        self._modules = {}

    def _instantiate_inner_class(self, hyper_name: str, sub_package: str) -> Union[object, None]:
        choice = self.hyperparams[hyper_name]["choice"]
        if choice != "none" and choice is not None:
            ClassObj = getattr(importlib.import_module(sub_package), choice)
            _duplicated_hypers = self.hyperparams[hyper_name].copy()
            if "choice" in list(_duplicated_hypers.keys()):
                del _duplicated_hypers["choice"]
            instance = ClassObj(**_duplicated_hypers)
        else:
            instance = None
        return instance

    def _adjust_hypers(self):

        hypers = self._hyperparams.copy()

        # Layer
        del hypers["previous_layer"]

        # kernel initializer
        hypers["kernel_initializer"] = self._instantiate_inner_class(
            "kernel_initializer", "keras.initializers")

        # bias initializer
        hypers["bias_initializer"] = self._instantiate_inner_class(
            "bias_initializer", "keras.initializers")

        # kernel regularizer
        hypers["kernel_regularizer"] = self._instantiate_inner_class(
            "kernel_regularizer", "keras.regularizers")

        # bias regularizer
        hypers["bias_regularizer"] = self._instantiate_inner_class(
            "bias_regularizer", "keras.regularizers")

        # activity regularizer
        hypers["activity_regularizer"] = self._instantiate_inner_class(
            "activity_regularizer", "keras.regularizers")

        # kernel constraint
        hypers["kernel_constraint"] = self._instantiate_inner_class(
            "kernel_constraint", "keras.constraints")

        # bias constraint
        hypers["bias_constraint"] = self._instantiate_inner_class(
            "bias_constraint", "keras.constraints")

        return hypers

    def get_neural_network_module(self, *, input_module: Module) -> Module:

        if input_module not in self._modules:

            previous_layer = self._hyperparams["previous_layer"]
            hypers = self._adjust_hypers()

            if previous_layer is not None:
                previous_layer_module = previous_layer.get_neural_network_module(
                    input_module=input_module)
            else:
                previous_layer_module = input_module
            current_layer_module = k_layers.Conv1D(
                **hypers)(previous_layer_module)

            self._modules[input_module] = current_layer_module

        return self._modules[input_module]
