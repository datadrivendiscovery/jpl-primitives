from tensorflow.keras import layers as k_layers
from tensorflow.keras.layers import Layer as KerasBaseLayer
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import PrimitiveBase, NeuralNetworkModuleMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m import utils
from typing import Dict, Union
from d3m.container import DataFrame as d3m_dataframe
import os
from collections import OrderedDict
from .null import Null
import importlib
from ..base_classes import LayerBase


class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):

    previous_layer = hyperparams.Union(
        OrderedDict({
            "previous_layer": hyperparams.Primitive[NeuralNetworkModuleMixin](
                default=Null,
                description='Composition of the neural network. Need to all implement NNLayerMixin',
                semantic_types=[
                    'https://metadata.datadrivendiscovery.org/types/TuningParameter']
            ),
            "none": hyperparams.Hyperparameter[None](
                default=None,
            )
        }),
        default='none',
        description='next layer in the neural network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    axis = hyperparams.Hyperparameter[int](
        default=-1,
        description="featuer axis to do the batch norm over",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )

    momentum = hyperparams.Bounded(
        default=0.99,
        lower=0.0,
        upper=1.0,
        description="Momentum for the moving mean and the moving variance.",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    epsilon = hyperparams.Bounded(
        default=0.001,
        lower=0.0,
        upper=1.0,
        description="Small float added to variance to avoid dividing by zero.",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    center = hyperparams.UniformBool(
        default=True,
        description="If True, add offset of beta to normalized tensor. If False, beta is ignored.",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    scale = hyperparams.UniformBool(
        default=True,
        description="If True, multiply by gamma. If False, gamma is not used. When the next layer is linear (also e.g. nn.relu), this can be disabled since the scaling will be done by the next layer.",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    beta_initializer = hyperparams.Choice(
        choices={
            'Zeros': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Ones': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Constant': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'stddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomUniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'minval': hyperparams.Hyperparameter[float](
                        default=-0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'maxval': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'TruncatedNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'steddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'VarianceScaling': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'scale': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'mode': hyperparams.Enumeration(
                        default="fan_in",
                        values=["fan_in", "fan_out", "fan_avg"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'distribution': hyperparams.Enumeration(
                        default="normal",
                        values=["normal", "uniform"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Orthogonal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Identity': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'lecun_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'lecun_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            )
        },
        default="Zeros",
        description='kernel initializer for the network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    gamma_initializer = hyperparams.Choice(
        choices={
            'Zeros': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Ones': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Constant': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'stddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomUniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'minval': hyperparams.Hyperparameter[float](
                        default=-0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'maxval': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'TruncatedNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'steddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'VarianceScaling': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'scale': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'mode': hyperparams.Enumeration(
                        default="fan_in",
                        values=["fan_in", "fan_out", "fan_avg"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'distribution': hyperparams.Enumeration(
                        default="normal",
                        values=["normal", "uniform"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Orthogonal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Identity': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'lecun_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'lecun_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            )
        },
        default="Ones",
        description='bias initializer for the network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    moving_mean_initializer = hyperparams.Choice(
        choices={
            'Zeros': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Ones': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Constant': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'stddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomUniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'minval': hyperparams.Hyperparameter[float](
                        default=-0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'maxval': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'TruncatedNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'steddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'VarianceScaling': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'scale': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'mode': hyperparams.Enumeration(
                        default="fan_in",
                        values=["fan_in", "fan_out", "fan_avg"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'distribution': hyperparams.Enumeration(
                        default="normal",
                        values=["normal", "uniform"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Orthogonal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Identity': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'lecun_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'lecun_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            )
        },
        default="Zeros",
        description='kernel initializer for the network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    moving_variance_initializer = hyperparams.Choice(
        choices={
            'Zeros': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Ones': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'Constant': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'stddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'RandomUniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'minval': hyperparams.Hyperparameter[float](
                        default=-0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'maxval': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'TruncatedNormal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'mean': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'steddev': hyperparams.Hyperparameter[float](
                        default=0.05,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'VarianceScaling': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'scale': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'mode': hyperparams.Enumeration(
                        default="fan_in",
                        values=["fan_in", "fan_out", "fan_avg"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'distribution': hyperparams.Enumeration(
                        default="normal",
                        values=["normal", "uniform"],
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Orthogonal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'Identity': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'gain': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'lecun_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'glorot_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'lecun_normal': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            ),
            'he_uniform': hyperparams.Hyperparams.define(
                configuration=OrderedDict({

                })
            )
        },
        default="Ones",
        description='kernel initializer for the network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    beta_regularizer = hyperparams.Choice(
        choices={
            'l1': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l1_l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l1': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'l2': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="kernel regularizer for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    gamma_regularizer = hyperparams.Choice(
        choices={
            'l1': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'l1_l2': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'l1': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'l2': hyperparams.Hyperparameter[float](
                        default=0.01,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="kernel regularizer for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    beta_constraint = hyperparams.Choice(
        choices={
            'MaxNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'max_value': hyperparams.Hyperparameter[int](
                        default=2,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'NonNeg': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'UnitNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'MinMaxNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'min_value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'max_value': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'rate': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="kernel constraint for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    gamma_constraint = hyperparams.Choice(
        choices={
            'MaxNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'max_value': hyperparams.Hyperparameter[int](
                        default=2,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'NonNeg': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            ),
            'UnitNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            'MinMaxNorm': hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                    'min_value': hyperparams.Hyperparameter[float](
                        default=0.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'max_value': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'rate': hyperparams.Hyperparameter[float](
                        default=1.0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    ),
                    'axis': hyperparams.Hyperparameter[int](
                        default=0,
                        semantic_types=[
                            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
                    )
                })
            ),
            "none": hyperparams.Hyperparams.define(
                configuration=OrderedDict({
                })
            )},
        default="none",
        description="bias constraint for the network layer",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )


Inputs = d3m_dataframe
Outputs = d3m_dataframe
Module = KerasBaseLayer


class BatchNormalization(LayerBase[Inputs, Outputs, Params, Hyperparams], NeuralNetworkModuleMixin[Inputs, Outputs, Params, Hyperparams, Module]):
    _orig = LayerBase.__doc__
    _general, _superclass = _orig.split("Pure Keras Documentation:\n\n")
    __doc__ = _general + "Pure Keras Documentation:\n\n" + \
        k_layers.BatchNormalization.__doc__ + "\n\n" + _superclass

    __author__ = "Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>"
    metadata = metadata_base.PrimitiveMetadata({
        'id': '288162cd-b71d-418d-9555-cc177c5f592e',
        'version': '1.2.0',
        'name': 'batch_normalization',
        'keywords': ['neural network', 'deep learning'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/keras_wrap/layers/batch_normalization.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.layer.batch_normalization.KerasWrap',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': metadata_base.PrimitiveFamily.LAYER,
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)
        self._hyperparams = hyperparams
        self._modules = {}

    def _instantiate_inner_class(self, hyper_name: str, sub_package: str) -> Union[object, None]:
        choice = self.hyperparams[hyper_name]["choice"]
        if choice != "none" and choice is not None:
            ClassObj = getattr(importlib.import_module(sub_package), choice)
            _duplicated_hypers = self.hyperparams[hyper_name].copy()
            if "choice" in list(_duplicated_hypers.keys()):
                del _duplicated_hypers["choice"]
            instance = ClassObj(**_duplicated_hypers)
        else:
            instance = None
        return instance

    def _adjust_hypers(self):

        hypers = self._hyperparams.copy()

        # Layer
        del hypers["previous_layer"]

        # For every hyperaram that needs to be separately instantiatied, we need to generally do so and then delete from our copied dictionary

        # beta initializer
        hypers["beta_initializer"] = self._instantiate_inner_class(
            "beta_initializer", "keras.initializers")

        # gamma initializer
        hypers["gamma_initializer"] = self._instantiate_inner_class(
            "gamma_initializer", "keras.initializers")

        # moving mean initializer
        hypers["moving_mean_initializer"] = self._instantiate_inner_class("moving_mean_initializer",
                                                                          "keras.initializers")

        # moving variance initializer
        hypers["moving_variance_initializer"] = self._instantiate_inner_class("moving_variance_initializer",
                                                                              "keras.initializers")

        # beta regularizer
        hypers["beta_regularizer"] = self._instantiate_inner_class(
            "beta_regularizer", "keras.regularizers")

        # gamma regularizer
        hypers["gamma_regularizer"] = self._instantiate_inner_class(
            "gamma_regularizer", "keras.regularizers")

        # beta constraint
        hypers["beta_constraint"] = self._instantiate_inner_class(
            "beta_constraint", "keras.constraints")

        # gamma constraint
        hypers["gamma_constraint"] = self._instantiate_inner_class(
            "gamma_constraint", "keras.constraints")

        return hypers

    def get_neural_network_module(self, *, input_module: Module) -> Module:

        if input_module not in self._modules:

            previous_layer = self._hyperparams["previous_layer"]
            hypers = self._adjust_hypers()

            if previous_layer is not None:
                previous_layer_module = previous_layer.get_neural_network_module(
                    input_module=input_module)
            else:
                previous_layer_module = input_module
            current_layer_module = k_layers.BatchNormalization(
                **hypers)(previous_layer_module)

            self._modules[input_module] = current_layer_module

        return self._modules[input_module]
