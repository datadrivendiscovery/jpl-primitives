from tensorflow.keras import layers as k_layers
from tensorflow.keras.layers import Layer as KerasBaseLayer
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import PrimitiveBase, NeuralNetworkModuleMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m import utils
from typing import Dict, List, Union, Tuple, Sequence
from d3m.container import DataFrame as d3m_dataframe
from d3m.container import List as d3m_list
import os
import collections
from .null import Null
from .dense import Dense
from ..base_classes import LayerBase


class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):

    previous_layers = hyperparams.Set(
        elements=hyperparams.Primitive[NeuralNetworkModuleMixin](
            default=Null,
            description='Composition of the neural network. Need to all implement NNLayerMixin',
            semantic_types=[
                'https://metadata.datadrivendiscovery.org/types/ControlParameter']
        ),
        min_size=2,
        max_size=None,
        default=[Null, Dense],
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )


Inputs = d3m_dataframe
Outputs = d3m_dataframe
Module = KerasBaseLayer


class Concatenate(LayerBase[Inputs, Outputs, Params, Hyperparams], NeuralNetworkModuleMixin[Inputs, Outputs, Params, Hyperparams, Module]):
    _orig = LayerBase.__doc__
    _general, _superclass = _orig.split("Pure Keras Documentation:\n\n")
    __doc__ = _general + "Pure Keras Documentation:\n\n" + \
        k_layers.Concatenate.__doc__ + "\n\n" + _superclass

    __author__ = "Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov"
    metadata = metadata_base.PrimitiveMetadata({
        'id': 'e790bc3c-5b37-4e38-adc9-87e58a2b26ed',
        'version': '1.2.0',
        'name': 'concat',
        'keywords': ['neural network', 'deep learning'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/keras_wrap/layers/concat.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.layer.concat.KerasWrap',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': metadata_base.PrimitiveFamily.LAYER,
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)
        self._hyperparams = hyperparams
        self._modules = {}

    def get_neural_network_module(self, *, input_module: Module) -> Module:
        """
        left_layer = self._hyperparams["left_layer"]
        right_layer = self._hyperparams["right_layer"]

        left_layer_module = left_layer.get_neural_network_module(upper_module=upper_module)
        right_layer_module = right_layer.get_neural_network_module(upper_module=upper_module)

        current_layer_module = k_layers.Add()([left_layer_module, right_layer_module])

        """
        if input_module not in self._modules:

            previous_layers = self._hyperparams["previous_layers"]

            previous_layer_modules = []
            for l in previous_layers:
                previous_layer_modules.append(
                    l.get_neural_network_module(input_module=input_module))

            current_layer_module = k_layers.Concatenate()(previous_layer_modules)

            self._modules[input_module] = current_layer_module

        return self._modules[input_module]
