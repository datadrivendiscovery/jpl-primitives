from tensorflow.keras import layers as k_layers
from tensorflow.keras.layers import Layer as KerasBaseLayer
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import PrimitiveBase, NeuralNetworkModuleMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m import utils
from typing import Dict
from d3m.container import DataFrame as d3m_dataframe
import os
from collections import OrderedDict
from .null import Null
from ..base_classes import LayerBase


class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):

    previous_layer = hyperparams.Union(
        OrderedDict({
            "previous_layer": hyperparams.Primitive[NeuralNetworkModuleMixin](
                default=Null,
                description='Composition of the neural network. Need to all implement NNLayerMixin',
                semantic_types=[
                    'https://metadata.datadrivendiscovery.org/types/TuningParameter']
            ),
            "none": hyperparams.Hyperparameter[None](
                default=None,
            )
        }),
        default='none',
        description='next layer in the neural network',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    rate = hyperparams.Bounded(
        default=0.2,
        lower=0.0,
        upper=0.99,
        description="float between 0 and 1. Fraction of the input units to drop.",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )


Inputs = d3m_dataframe
Outputs = d3m_dataframe
Module = KerasBaseLayer


class Dropout(LayerBase[Inputs, Outputs, Params, Hyperparams], NeuralNetworkModuleMixin[Inputs, Outputs, Params, Hyperparams, Module]):
    _orig = LayerBase.__doc__
    _general, _superclass = _orig.split("Pure Keras Documentation:\n\n")
    __doc__ = _general + "Pure Keras Documentation:\n\n" + \
        k_layers.Dropout.__doc__ + "\n\n" + _superclass

    __author__ = "Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>"
    metadata = metadata_base.PrimitiveMetadata({
        'id': '7a76922c-bf6f-37ce-9e58-1d3315382506',
        'version': '1.2.0',
        'name': 'dropout',
        'keywords': ['neural network', 'deep learning'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/keras_wrap/layers/dropout.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.layer.dropout.KerasWrap',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': metadata_base.PrimitiveFamily.LAYER,
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)
        self._hyperparams = hyperparams
        self._modules = {}

    def _adjust_hypers(self):

        hypers = self._hyperparams.copy()

        # Layer
        del hypers["previous_layer"]

        return hypers

    def get_neural_network_module(self, *, input_module: Module) -> Module:

        if input_module not in self._modules:

            previous_layer = self._hyperparams["previous_layer"]
            hypers = self._adjust_hypers()

            if previous_layer is not None:
                previous_layer_module = previous_layer.get_neural_network_module(
                    input_module=input_module)
            else:
                previous_layer_module = input_module
            current_layer_module = k_layers.Dropout(
                **hypers)(previous_layer_module)

            self._modules[input_module] = current_layer_module

        return self._modules[input_module]
