import torchvision
from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator
import torch
import random

from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

from d3m.primitive_interfaces.base import ContinueFitMixin, GradientCompositionalityMixin
from d3m import utils as d3m_utils
from d3m.container import DataFrame as d3m_dataframe
from d3m.container.pandas import DataFrame
import d3m.metadata.base as metadata_module
import d3m
from d3m.base import utils as base_utils
from jpl_primitives import utils


from typing import List, Union, Dict, Tuple, Optional
import os
import typing
import numpy as np
import pandas as pd


from collections import OrderedDict
import sys
import importlib


class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):

    classes = hyperparams.Enumeration[str](
        values=['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', ' truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign',
                'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear', 'zebra', 'giraffe', 'backpack',
                'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
                'skateboard', 'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
                'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed', 'dining table',
                'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book',
                'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush'],
        default='person',
        description='Class you want to do inference on',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
    )

    confidence = hyperparams.Hyperparameter[float](
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        default=0.9,
        description='Detection threshold for how confident our predictions are.'
    )

    use_gpu = hyperparams.UniformBool(
        default=True,
        description="Whether or not to use the gpu for computation",
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )

    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    use_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )

    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        # Default value depends on the nature of the primitive.
        default='append',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should resulting columns be appended, should they replace original columns, or should only resulting columns be returned?",
    )

    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )


Inputs = d3m_dataframe
Outputs = d3m_dataframe


class FasterRCNNTorch(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams],
                      ContinueFitMixin[Inputs, Outputs, Params, Hyperparams]):
    """
    Torchvision implemenation of faster_rcnn
    """
    __author__ = "Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>"
    metadata = metadata_module.PrimitiveMetadata({
        'id': 'ad740ad2-d318-4b42-abe4-896b8bb8dd84',
        'version': '0.1.0',
        'name': 'faster_rcnn',
        'keywords': ['neural network', 'deep learning', 'object detection'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/object_detection/faster_rcnn.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(git_commit=d3m.utils.current_git_commit(os.path.dirname(__file__)))
        },
            {'type': metadata_module.PrimitiveInstallationType.FILE,
             'key': 'coco_pretrained_weights.pth',
             'file_uri': 'http://public.datadrivendiscovery.org/fasterrcnn_resnet50_fpn_coco-258fb6c6.pth',
             'file_digest': '258fb6c638b15964ddcdd1ae0748c5eef1be9e732750120cc857feed3faac384'}
        ],
        'python_path': 'd3m.primitives.object_detection.faster_rcnn.JPLPrimitives',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': 'OBJECT_DETECTION',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 volumes: Union[Dict[str, str], None] = None,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed,
                         docker_containers=docker_containers)

        self.hyperparams = hyperparams
        self.random_seed = random_seed
        self._volumes = volumes
        #self._volumes2 = {'coco_weights': 'coco_pretrained_weights.pth'}
        self._idx_to_label, self._label_to_idx = self._load_class_dicts()
        self._model = self._build_model()

    def _build_model(self):

        def fasterrcnn_resnet50_fpn(num_classes=91, **kwargs):

            # forcing pretrained_backbone false so we don't download, but we are going to be loading
            # full pretrained weights anyways so it doesn't matter
            pretrained_backbone = False
            backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone(
                'resnet50', pretrained_backbone)
            model = torchvision.models.detection.FasterRCNN(
                backbone, num_classes, **kwargs)

            print(self._volumes, file=sys.__stdout__)
            model.load_state_dict(torch.load(
                self._volumes['coco_pretrained_weights.pth']))
            # weights_path = self.hyperparams['weights_path']
            # if weights_path is not None:
            #     # local weights path for reference: '/Users/markh/.cache/torch/checkpoints/fasterrcnn_resnet50_fpn_coco-258fb6c6.pth'
            #     model.load_state_dict(torch.load(weights_path))

            return model

        model = fasterrcnn_resnet50_fpn()

        return model

    def _load_class_dicts(self):
        idx_to_label = {0: u'__background__',
                        1: u'person',
                        2: u'bicycle',
                        3: u'car',
                        4: u'motorcycle',
                        5: u'airplane',
                        6: u'bus',
                        7: u'train',
                        8: u'truck',
                        9: u'boat',
                        10: u'traffic light',
                        11: u'fire hydrant',
                        12: u'stop sign',
                        13: u'parking meter',
                        14: u'bench',
                        15: u'bird',
                        16: u'cat',
                        17: u'dog',
                        18: u'horse',
                        19: u'sheep',
                        20: u'cow',
                        21: u'elephant',
                        22: u'bear',
                        23: u'zebra',
                        24: u'giraffe',
                        25: u'backpack',
                        26: u'umbrella',
                        27: u'handbag',
                        28: u'tie',
                        29: u'suitcase',
                        30: u'frisbee',
                        31: u'skis',
                        32: u'snowboard',
                        33: u'sports ball',
                        34: u'kite',
                        35: u'baseball bat',
                        36: u'baseball glove',
                        37: u'skateboard',
                        38: u'surfboard',
                        39: u'tennis racket',
                        40: u'bottle',
                        41: u'wine glass',
                        42: u'cup',
                        43: u'fork',
                        44: u'knife',
                        45: u'spoon',
                        46: u'bowl',
                        47: u'banana',
                        48: u'apple',
                        49: u'sandwich',
                        50: u'orange',
                        51: u'broccoli',
                        52: u'carrot',
                        53: u'hot dog',
                        54: u'pizza',
                        55: u'donut',
                        56: u'cake',
                        57: u'chair',
                        58: u'couch',
                        59: u'potted plant',
                        60: u'bed',
                        61: u'dining table',
                        62: u'toilet',
                        63: u'tv',
                        64: u'laptop',
                        65: u'mouse',
                        66: u'remote',
                        67: u'keyboard',
                        68: u'cell phone',
                        69: u'microwave',
                        70: u'oven',
                        71: u'toaster',
                        72: u'sink',
                        73: u'refrigerator',
                        74: u'book',
                        75: u'clock',
                        76: u'vase',
                        77: u'scissors',
                        78: u'teddy bear',
                        79: u'hair drier',
                        80: u'toothbrush'}

        label_to_idx = {val: key for key, val in idx_to_label.items()}
        return idx_to_label, label_to_idx

    """
    SupervisedLearnerPrimitiveBase methods, see: https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/primitive_interfaces/base.py


    """

    def _filter_above_confidence(self, preds, confidence=0.9):

        new_preds = []
        for img in preds:
            _img = img.copy()
            idxs_val = [i for i, score in enumerate(
                _img['scores']) if score >= confidence]
            _img['boxes'] = _img['boxes'][idxs_val]
            _img['labels'] = _img['labels'][idxs_val]
            _img['scores'] = _img['scores'][idxs_val]
            new_preds.append(_img)

        return new_preds

    def _filter_by_class(self, preds, classes):
        class_idxs = set()
        for c in classes:
            if self._label_to_idx.get(c) is not None:
                class_idxs.add(self._label_to_idx.get(c))
        class_idxs = list(class_idxs)

        new_preds = []
        for img in preds:
            _img = img.copy()
            idxs_val = [i for i, label_idx in enumerate(
                _img['labels']) if label_idx in class_idxs]
            _img['boxes'] = _img['boxes'][idxs_val]
            _img['labels'] = _img['labels'][idxs_val]
            _img['scores'] = _img['scores'][idxs_val]
            new_preds.append(_img)
        return new_preds

    def _produce(self, input_df, model):

        def format_bounding_box(bbox):
            """
            x0, y0, x1, y1 format --> d3m point polygon format
            """
            x0, y0, x1, y1 = int(bbox[0]), int(
                bbox[1]), int(bbox[2]), int(bbox[3])

            # return f"{x0},{y0},{x1},{y0},{x1},{y1},{x0},{y1}" # X coords do not match
            return f"{x0},{y0},{x0},{y1},{x1},{y1},{x1},{y0}"

        input_df = input_df.drop_duplicates(subset=['d3mIndex'])

        d3m_index = input_df['d3mIndex'].tolist()
        data = [torch.from_numpy(x / 255.0).float().permute(2, 0, 1)
                for x in input_df.iloc[:, -1].values]

        _output_df = DataFrame(
            columns=['d3mIndex', 'bounding_box', 'class', 'confidence'])
        for idx, img in zip(d3m_index, data):
            print(f"processing d3mIndex: {idx}", file=sys.__stdout__)
            predictions = model([img])
            preds = self._filter_above_confidence(
                predictions, confidence=self.hyperparams['confidence'])
            preds = self._filter_by_class(
                preds, [self.hyperparams['classes']])

            for p in preds:
                if len(p['boxes']) != 0:
                    for i in range(len(p['boxes'])):
                        out_bbox = p['boxes'][i]
                        out_class = int(p['labels'][i])
                        out_confidence = float(p['scores'][i])
                        _output_df = _output_df.append([pd.Series({'d3mIndex': idx,
                                                                   'bounding_box': format_bounding_box(out_bbox),
                                                                   'class': self._idx_to_label[out_class],
                                                                   'confidence':out_confidence})])
                else:
                    _output_df = _output_df.append([pd.Series({'d3mIndex': idx,
                                                               'bounding_box': "0,0,0,0,0,0,0,0",
                                                               'class': 'background',
                                                               'confidence': 0.0})])

        return _output_df

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:

        selected_inputs, columns_to_use = self._select_inputs_columns(
            self._training_inputs)

        with torch.random.fork_rng() as sess:
            torch.manual_seed(self.random_seed)
            self._model.eval()
            print(f"SELECTED_INPUTS: {selected_inputs}")
            print(f"COLUMNS TO USE: {columns_to_use}")

            outputs = self._produce(inputs, self._model)

            # For now we are assuming just 1 class so we remove class column
            outputs = outputs.drop(columns=['class'])

            # output_columns = [self._wrap_predictions(predictions)]

            # outputs = base_utils.combine_columns(inputs, columns_to_use, output_columns,
            #                                      return_result=self.hyperparams['return_result'],
            #                                      add_index_columns=self.hyperparams['add_index_columns'])

        return CallResult(outputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        """

        """
        self._training_inputs = inputs
        self._training_outputs = outputs

        return

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        """
        pass
        """

        # Ignoring training for now
        # inputs, _ = self._select_inputs_columns(self._training_inputs)
        # outputs, _ = self._select_outputs_columns(self._training_outputs)

        # self._store_columns_metadata_and_names(inputs, outputs)

        return CallResult(None)

    def get_params(self) -> Params:
        pass

    def set_params(self, *, params: Params) -> None:
        pass

    """
    ContinueFitMixin methods, see: https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/primitive_interfaces/base.py


    """

    def continue_fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:

        # Ignoring training for now...

        # inputs, _ = self._select_inputs_columns(self._training_inputs)
        # outputs, _ = self._select_outputs_columns(self._training_outputs)

        return CallResult(None)

    """
    Helper methods (Will be moved to SupervisedLearner Base class eventually
    
    """

    @classmethod
    def _can_use_inputs_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = inputs_metadata.query(
            (metadata_base.ALL_ELEMENTS, column_index))

        accepted_structural_types = (np.ndarray)
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        return 'https://metadata.datadrivendiscovery.org/types/Attribute' in column_metadata.get('semantic_types', [])

    @classmethod
    def _get_inputs_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_inputs_column(inputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(inputs_metadata,
                                                                      hyperparams['use_inputs_columns'],
                                                                      hyperparams['exclude_inputs_columns'],
                                                                      can_use_column)

        if not columns_to_use:
            raise ValueError("No inputs columns.")

        if hyperparams['use_inputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified inputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    @classmethod
    def _can_use_outputs_column(cls, outputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = outputs_metadata.query(
            (metadata_base.ALL_ELEMENTS, column_index))

        return 'https://metadata.datadrivendiscovery.org/types/SuggestedTarget' in column_metadata.get('semantic_types',
                                                                                                       [])

    @classmethod
    def _get_outputs_columns(cls, outputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_outputs_column(outputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(outputs_metadata,
                                                                      hyperparams['use_outputs_columns'],
                                                                      hyperparams['exclude_outputs_columns'],
                                                                      can_use_column)

        if not columns_to_use:
            raise ValueError("No outputs columns.")

        if hyperparams['use_outputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified outputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    def _select_inputs_columns(self, inputs: Inputs) -> Tuple[Inputs, List[int]]:
        columns_to_use = self._get_inputs_columns(
            inputs.metadata, self.hyperparams)

        return utils.select_columns(inputs, columns_to_use, source=self), columns_to_use

    def _select_outputs_columns(self, outputs: Outputs) -> Tuple[Outputs, List[int]]:
        columns_to_use = self._get_outputs_columns(
            outputs.metadata, self.hyperparams)

        return utils.select_columns(outputs, columns_to_use, source=self), columns_to_use

    @classmethod
    def _add_target_semantic_types(cls, metadata: metadata_base.DataMetadata,
                                   source: typing.Any, target_names: List = None, ) -> metadata_base.DataMetadata:
        for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/Target',
                                                  source=source)
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                                                  source=source)
            if target_names:
                metadata = metadata.update((metadata_base.ALL_ELEMENTS, column_index), {
                    'name': target_names[column_index],
                }, source=source)
        return metadata

    def _wrap_predictions(self, predictions: np.ndarray) -> Outputs:
        outputs = d3m_dataframe(predictions, generate_metadata=False)
        outputs.metadata = self._update_predictions_metadata(
            outputs, self._target_columns_metadata)
        outputs.columns = self._target_columns_names
        return outputs

    @classmethod
    def _update_predictions_metadata(cls, outputs: Optional[Outputs],
                                     target_columns_metadata: List[OrderedDict]) -> metadata_base.DataMetadata:
        outputs_metadata = metadata_base.DataMetadata()
        if outputs is not None:
            outputs_metadata = outputs_metadata.generate(outputs)

        for column_index, column_metadata in enumerate(target_columns_metadata):
            outputs_metadata = outputs_metadata.update_column(
                column_index, column_metadata)

        return outputs_metadata

    def _store_columns_metadata_and_names(self, inputs: Inputs, outputs: Outputs) -> None:
        self._attribute_columns_names = list(inputs.columns)
        self._target_columns_metadata = self._get_target_columns_metadata(
            outputs.metadata)
        self._target_columns_names = list(outputs.columns)

    @classmethod
    def _get_target_columns_metadata(cls, outputs_metadata: metadata_base.DataMetadata) -> List[OrderedDict]:
        outputs_length = outputs_metadata.query((metadata_base.ALL_ELEMENTS,))[
            'dimension']['length']

        target_columns_metadata: List[OrderedDict] = []
        for column_index in range(outputs_length):
            column_metadata = OrderedDict(
                outputs_metadata.query_column(column_index))

            # Update semantic types and prepare it for predicted targets.
            semantic_types = list(column_metadata.get('semantic_types', []))
            if 'https://metadata.datadrivendiscovery.org/types/PredictedTarget' not in semantic_types:
                semantic_types.append(
                    'https://metadata.datadrivendiscovery.org/types/PredictedTarget')
            semantic_types = [semantic_type for semantic_type in semantic_types if
                              semantic_type != 'https://metadata.datadrivendiscovery.org/types/TrueTarget']
            column_metadata['semantic_types'] = semantic_types

            target_columns_metadata.append(column_metadata)

        return target_columns_metadata
