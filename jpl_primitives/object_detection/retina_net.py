from d3m.container.numpy import ndarray
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.container.pandas import DataFrame
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
import d3m
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, GradientCompositionalityMixin, Gradients, DockerContainer, ContinueFitMixin
from fastai.vision.models.unet import _get_sfs_idxs, model_sizes, hook_outputs
#import common_primitives
import collections
from collections import OrderedDict
from jpl_primitives import utils
from d3m.container.dataset import Dataset
#from .utils import to_variable
import common_primitives
from typing import Dict, List, Tuple, Type, Collection, Optional, Union
import os
import math
import typing
import sys
from fastai.vision import ObjectItemList, get_transforms, bb_pad_collate, Sizes, LossFunction, create_body, models, \
                            conv2d, conv_layer, ifnone, Learner, is_tuple, FloatTensor, tensor, LongTensor, ImageList, \
                            range_of, listify, flatten_model, partial, Recorder
from torch import nn
import torch.nn.functional as F
import fastai
import torch
import random
import numpy as np
import pandas as pd
import sys
from pathlib import Path

Inputs = DataFrame
Outputs = DataFrame


class Params(params.Params):
    state: Dict


class Hyperparams(hyperparams.Hyperparams):

    batch_size = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=64,
        description='Batch size.'
    )

    img_size = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=128,
        description='Image size.'
    )

    learning_rate = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1e-4,
        description='Learning rate used during training (fit).'
    )

    detect_threshold = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0.99,
        description='Detection threshold for how confident our predictions are.'
    )

    nms_threshold = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0.25,
        description='Threshold for nms process of filtering down of overlapping bounding boxes.'
    )

    use_gpu = hyperparams.Hyperparameter[bool](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=True,
        description='Whether or not to use GPU for compute. This should ALWAYS be turned on.'
    )

    # Default hyperparameters for general primitive behavior
    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    use_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )

    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        # Default value depends on the nature of the primitive.
        default='append',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should resulting columns be appended, should they replace original columns, or should only resulting columns be returned?",
    )



class RetinaNet(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams], ContinueFitMixin[Inputs, Outputs, Params, Hyperparams]):
    """
    Implementation of RetinaNet for object detection https://arxiv.org/abs/1708.02002
    """

    __author__ = 'Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>'
    metadata = metadata_module.PrimitiveMetadata({
        'id': '176e7277-fe37-402b-86e6-bdb7f7a9cf89',
        'version': '0.2.0',
        'name': 'retina_net',
        'keywords': ['neural network', 'deep learning', 'object detection'],
        'source': {
            'name': 'JPL-manual',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/jpl-primitives/blob/master/jpl_primitives/object_detection/retina_net.py',
                'https://gitlab.com/datadrivendiscovery/jpl-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/jpl-primitives.git@{git_commit}#egg=jpl_primitives'.format(git_commit=d3m.utils.current_git_commit(os.path.dirname(__file__)))
        },
            {'type': metadata_module.PrimitiveInstallationType.FILE,
             'key': 'resnet50-19c8e357.pth',
             'file_uri': 'http://public.datadrivendiscovery.org/resnet50-19c8e357.pth',
             'file_digest': '19c8e3572231adff6824a2da93fd67b5986919a2e65f8b6007eab4edee220097'}
        ],
        'python_path': 'd3m.primitives.object_detection.retina_net.JPLPrimitives',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': 'OBJECT_DETECTION',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 volumes: Union[Dict[str, str], None] = None,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        fastai.torch_core.defaults.device = torch.device('cuda') if self.hyperparams['use_gpu'] else torch.device('cpu')
        self.random_seed = random_seed
        self._volumes = volumes

    def _get_annotations_from_d3m_dataframe(self, df):

        def get_categories_from_dataframe(df, class_col_name):
            try:
                unique_classes = df[class_col_name].unique().tolist()
                cat_list = []
                for idx, c in enumerate(unique_classes):
                    cat_list.append({"id":idx+1,"name":c})
            except:
                cat_list = [{"id":1,"name":"cat1"}]
            return cat_list

        def get_images_from_dataframe(df, image_col_name):
            unique_photos = df[image_col_name].unique().tolist()
            images = []
            for idx, photo in enumerate(unique_photos):
                images.append({"file_name":photo,"id":idx})
            return images

        def lookup_id_by_key(match, key, l):
            return [d["id"] for d in l if d[key] == match][0]


        def convert_polygon_coords_to_coco_format(coords):
            # d3m format --> coco format
            #"x11,y11,x12,y12,x13,y13,x14,y14" --> (x, y, width, height)
            """
            TODO: Need to come back to this in order to do generic from Polygon. This is a short cut because we know for
            the object detection tasks in d3m as they currently stand we are storing as "polygon" even though we know it's really
            just a box with 4 coordinates...
            """
            #print(coords,file=sys.__stdout__)
            points = [int(v) for v in coords]
            x = min(points[::2])
            y = min(points[1::2])
            width = abs(points[0] - points[4])
            height = abs(points[1] - points[5])
            return [x, y, width, height]

        def get_annotations_from_dataframe(df, class_col_name, image_col_name, bbox_col_name):
            # if not self.hyperparams['use_semantic_types']:
            #     return
            # error statements if more than 1 column with that semantic type
            #print(df.metadata.pretty_print())
            #print(df.metadata.pretty_print(), file=sys.__stdout__)

            #print("col names:", file=sys.__stdout__)
            #print(f"{bbox_col_name} : {image_col_name} : {class_col_name}", file=sys.__stdout__)
            annotations = []
            #print(df.head(), file=sys.__stdout__)
            for idx, row in df.iterrows():
                current_annot = {}
                if row[image_col_name] == "":
                    break
                try:
                    current_annot["category_id"] = lookup_id_by_key(row[class_col_name], "name", get_categories_from_dataframe(df, class_col_name))
                except:
                    current_annot["category_id"] = 1
                current_annot["image_id"] = lookup_id_by_key(row[image_col_name], "file_name", get_images_from_dataframe(df, image_col_name))
                #print(row, file=sys.__stdout__)
                current_annot["bbox"] = convert_polygon_coords_to_coco_format(row[bbox_col_name])
                annotations.append(current_annot)
            return annotations



        #Process D3M to standard COCO form
        annot_dict = {"categories":[],"annotations":[], "images":[]}
        annot_dict["categories"] = get_categories_from_dataframe(df, self._class_col_name)
        annot_dict["annotations"] = get_annotations_from_dataframe(df, self._class_col_name, self._image_col_name, self._bbox_col_name)
        annot_dict["images"] = get_images_from_dataframe(df, self._image_col_name)

        # Return processed info
        id2images, id2bboxes, id2cats = {}, collections.defaultdict(list), collections.defaultdict(list)
        classes = {}
        #print(annot_dict['categories'], file=sys.__stdout__)
        for o in annot_dict['categories']:
            classes[o['id']] = o['name']
        for o in annot_dict['annotations']:
            #print(o,file=sys.__stdout__)
            bb = o['bbox']
            id2bboxes[o['image_id']].append([bb[1],bb[0], bb[3]+bb[1], bb[2]+bb[0]])
            id2cats[o['image_id']].append(classes[o['category_id']])
        for o in annot_dict['images']:
            if o['id'] in id2bboxes:
                id2images[o['id']] = o['file_name']
        ids = list(id2images.keys())
        return [id2images[k] for k in ids], [[id2bboxes[k], id2cats[k]] for k in ids]

    def _create_learner(self):
        class LateralUpsampleMerge(nn.Module):
            "Merge the features coming from the downsample path (in `hook`) with the upsample path."
            def __init__(self, ch, ch_lat, hook):
                super().__init__()
                self.hook = hook
                self.conv_lat = conv2d(ch_lat, ch, ks=1, bias=True)

            def forward(self, x):
                return self.conv_lat(self.hook.stored) + F.interpolate(x, self.hook.stored.shape[-2:], mode='nearest')

        class RetinaNet(nn.Module):
            "Implements RetinaNet from https://arxiv.org/abs/1708.02002"
            def __init__(self, encoder:nn.Module, n_classes, final_bias=0., chs=256, n_anchors=9, flatten=True):
                super().__init__()
                self.n_classes,self.flatten = n_classes,flatten
                imsize = (256,256)
                sfs_szs = model_sizes(encoder, size=imsize)
                sfs_idxs = list(reversed(_get_sfs_idxs(sfs_szs)))
                self.sfs = hook_outputs([encoder[i] for i in sfs_idxs])
                self.encoder = encoder
                self.c5top5 = conv2d(sfs_szs[-1][1], chs, ks=1, bias=True)
                self.c5top6 = conv2d(sfs_szs[-1][1], chs, stride=2, bias=True)
                self.p6top7 = nn.Sequential(nn.ReLU(), conv2d(chs, chs, stride=2, bias=True))
                self.merges = nn.ModuleList([LateralUpsampleMerge(chs, sfs_szs[idx][1], hook)
                                             for idx,hook in zip(sfs_idxs[-2:-4:-1], self.sfs[-2:-4:-1])])
                self.smoothers = nn.ModuleList([conv2d(chs, chs, 3, bias=True) for _ in range(3)])
                self.classifier = self._head_subnet(n_classes, n_anchors, final_bias, chs=chs)
                self.box_regressor = self._head_subnet(4, n_anchors, 0., chs=chs)

            def _head_subnet(self, n_classes, n_anchors, final_bias=0., n_conv=4, chs=256):
                "Helper function to create one of the subnet for regression/classification."
                layers = [conv_layer(chs, chs, bias=True, norm_type=None) for _ in range(n_conv)]
                layers += [conv2d(chs, n_classes * n_anchors, bias=True)]
                layers[-1].bias.data.zero_().add_(final_bias)
                layers[-1].weight.data.fill_(0)
                return nn.Sequential(*layers)

            def _apply_transpose(self, func, p_states, n_classes):
                #Final result of the classifier/regressor is bs * (k * n_anchors) * h * w
                #We make it bs * h * w * n_anchors * k then flatten in bs * -1 * k so we can contenate
                #all the results in bs * anchors * k (the non flatten version is there for debugging only)
                if not self.flatten:
                    sizes = [[p.size(0), p.size(2), p.size(3)] for p in p_states]
                    return [func(p).permute(0,2,3,1).view(*sz,-1,n_classes) for p,sz in zip(p_states,sizes)]
                else:
                    return torch.cat([func(p).permute(0,2,3,1).contiguous().view(p.size(0),-1,n_classes) for p in p_states],1)

            def forward(self, x):
                c5 = self.encoder(x)
                p_states = [self.c5top5(c5.clone()), self.c5top6(c5)]
                p_states.append(self.p6top7(p_states[-1]))
                for merge in self.merges: p_states = [merge(p_states[0])] + p_states
                for i, smooth in enumerate(self.smoothers[:3]):
                    p_states[i] = smooth(p_states[i])
                return [self._apply_transpose(self.classifier, p_states, self.n_classes),
                        self._apply_transpose(self.box_regressor, p_states, 4),
                        [[p.size(2), p.size(3)] for p in p_states]]

            def __del__(self):
                if hasattr(self, "sfs"): self.sfs.remove()

        def create_grid(size):
            "Create a grid of a given `size`."
            H, W = size if is_tuple(size) else (size,size)
            grid = FloatTensor(H, W, 2)
            linear_points = torch.linspace(-1+1/W, 1-1/W, W) if W > 1 else tensor([0.])
            grid[:, :, 1] = torch.ger(torch.ones(H), linear_points).expand_as(grid[:, :, 0])
            linear_points = torch.linspace(-1+1/H, 1-1/H, H) if H > 1 else tensor([0.])
            grid[:, :, 0] = torch.ger(linear_points, torch.ones(W)).expand_as(grid[:, :, 1])
            return grid.view(-1,2)

        def create_anchors(sizes, ratios, scales, flatten=True):
            "Create anchor of `sizes`, `ratios` and `scales`."
            aspects = [[[s*math.sqrt(r), s*math.sqrt(1/r)] for s in scales] for r in ratios]
            aspects = torch.tensor(aspects).view(-1,2)
            anchors = []
            for h,w in sizes:
                #4 here to have the anchors overlap.
                sized_aspects = 4 * (aspects * torch.tensor([2/h,2/w])).unsqueeze(0)
                base_grid = create_grid((h,w)).unsqueeze(1)
                n,a = base_grid.size(0),aspects.size(0)
                ancs = torch.cat([base_grid.expand(n,a,2), sized_aspects.expand(n,a,2)], 2)
                anchors.append(ancs.view(h,w,a,4))
            return torch.cat([anc.view(-1,4) for anc in anchors],0) if flatten else anchors



        def activ_to_bbox(acts, anchors, flatten=True):
            "Extrapolate bounding boxes on anchors from the model activations."
            if flatten:
                acts.mul_(acts.new_tensor([[0.1, 0.1, 0.2, 0.2]])) #Can't remember where those scales come from, but they help regularize
                centers = anchors[...,2:] * acts[...,:2] + anchors[...,:2]
                sizes = anchors[...,2:] * torch.exp(acts[...,:2])
                return torch.cat([centers, sizes], -1)
            else: return [activ_to_bbox(act,anc) for act,anc in zip(acts, anchors)]
            return res

        def cthw2tlbr(boxes):
            "Convert center/size format `boxes` to top/left bottom/right corners."
            top_left = boxes[:,:2] - boxes[:,2:]/2
            bot_right = boxes[:,:2] + boxes[:,2:]/2
            return torch.cat([top_left, bot_right], 1)

        def intersection(anchors, targets):
            "Compute the sizes of the intersections of `anchors` by `targets`."
            ancs, tgts = cthw2tlbr(anchors), cthw2tlbr(targets)
            a, t = ancs.size(0), tgts.size(0)
            ancs, tgts = ancs.unsqueeze(1).expand(a,t,4), tgts.unsqueeze(0).expand(a,t,4)
            top_left_i = torch.max(ancs[...,:2], tgts[...,:2])
            bot_right_i = torch.min(ancs[...,2:], tgts[...,2:])
            sizes = torch.clamp(bot_right_i - top_left_i, min=0)
            return sizes[...,0] * sizes[...,1]

        def IoU_values(anchors, targets):
            "Compute the IoU values of `anchors` by `targets`."
            inter = intersection(anchors, targets)
            anc_sz, tgt_sz = anchors[:,2] * anchors[:,3], targets[:,2] * targets[:,3]
            union = anc_sz.unsqueeze(1) + tgt_sz.unsqueeze(0) - inter
            return inter/(union+1e-8)

        def match_anchors(anchors, targets, match_thr=0.5, bkg_thr=0.4):
            "Match `anchors` to targets. -1 is match to background, -2 is ignore."
            matches = anchors.new(anchors.size(0)).zero_().long() - 2
            if targets.numel() == 0: return matches
            ious = IoU_values(anchors, targets)
            vals,idxs = torch.max(ious,1)
            matches[vals < bkg_thr] = -1
            matches[vals > match_thr] = idxs[vals > match_thr]
            return matches

        def tlbr2cthw(boxes):
            "Convert top/left bottom/right format `boxes` to center/size corners."
            center = (boxes[:,:2] + boxes[:,2:])/2
            sizes = boxes[:,2:] - boxes[:,:2]
            return torch.cat([center, sizes], 1)

        def bbox_to_activ(bboxes, anchors, flatten=True):
            "Return the target of the model on `anchors` for the `bboxes`."
            if flatten:
                t_centers = (bboxes[...,:2] - anchors[...,:2]) / anchors[...,2:]
                t_sizes = torch.log(bboxes[...,2:] / anchors[...,2:] + 1e-8)
                return torch.cat([t_centers, t_sizes], -1).div_(bboxes.new_tensor([[0.1, 0.1, 0.2, 0.2]]))
            else: return [activ_to_bbox(act,anc) for act,anc in zip(acts, anchors)]
            return res

        def encode_class(idxs, n_classes):
            target = idxs.new_zeros(len(idxs), n_classes).float()
            mask = idxs != 0
            i1s = LongTensor(list(range(len(idxs))))
            target[i1s[mask],idxs[mask]-1] = 1
            return target

        class RetinaNetFocalLoss(nn.Module):

            def __init__(self, gamma:float=2., alpha:float=0.25,  pad_idx:int=0, scales:Collection[float]=None,
                         ratios:Collection[float]=None, reg_loss:LossFunction=F.smooth_l1_loss):
                super().__init__()
                self.gamma,self.alpha,self.pad_idx,self.reg_loss = gamma,alpha,pad_idx,reg_loss
                self.scales = ifnone(scales, [1,2**(-1/3), 2**(-2/3)])
                self.ratios = ifnone(ratios, [1/2,1,2])

            def _change_anchors(self, sizes:Sizes) -> bool:
                if not hasattr(self, 'sizes'): return True
                for sz1, sz2 in zip(self.sizes, sizes):
                    if sz1[0] != sz2[0] or sz1[1] != sz2[1]: return True
                return False

            def _create_anchors(self, sizes:Sizes, device:torch.device):
                self.sizes = sizes
                self.anchors = create_anchors(sizes, self.ratios, self.scales).to(device)

            def _unpad(self, bbox_tgt, clas_tgt):
                i = torch.min(torch.nonzero(clas_tgt-self.pad_idx))
                return tlbr2cthw(bbox_tgt[i:]), clas_tgt[i:]-1+self.pad_idx

            def _focal_loss(self, clas_pred, clas_tgt):
                encoded_tgt = encode_class(clas_tgt, clas_pred.size(1))
                ps = torch.sigmoid(clas_pred.detach())
                weights = encoded_tgt * (1-ps) + (1-encoded_tgt) * ps
                alphas = (1-encoded_tgt) * self.alpha + encoded_tgt * (1-self.alpha)
                weights.pow_(self.gamma).mul_(alphas)
                clas_loss = F.binary_cross_entropy_with_logits(clas_pred, encoded_tgt, weights, reduction='sum')
                return clas_loss

            def _one_loss(self, clas_pred, bbox_pred, clas_tgt, bbox_tgt):
                bbox_tgt, clas_tgt = self._unpad(bbox_tgt, clas_tgt)
                matches = match_anchors(self.anchors, bbox_tgt)
                bbox_mask = matches>=0
                if bbox_mask.sum() != 0:
                    bbox_pred = bbox_pred[bbox_mask]
                    bbox_tgt = bbox_tgt[matches[bbox_mask]]
                    bb_loss = self.reg_loss(bbox_pred, bbox_to_activ(bbox_tgt, self.anchors[bbox_mask]))
                else: bb_loss = 0.
                matches.add_(1)
                clas_tgt = clas_tgt + 1
                clas_mask = matches>=0
                clas_pred = clas_pred[clas_mask]
                clas_tgt = torch.cat([clas_tgt.new_zeros(1).long(), clas_tgt])
                clas_tgt = clas_tgt[matches[clas_mask]]
                return bb_loss + self._focal_loss(clas_pred, clas_tgt)/torch.clamp(bbox_mask.sum(), min=1.)

            def forward(self, output, bbox_tgts, clas_tgts):
                clas_preds, bbox_preds, sizes = output
                if self._change_anchors(sizes): self._create_anchors(sizes, clas_preds.device)
                n_classes = clas_preds.size(2)
                return sum([self._one_loss(cp, bp, ct, bt)
                            for (cp, bp, ct, bt) in zip(clas_preds, bbox_preds, clas_tgts, bbox_tgts)])/clas_tgts.size(0)

        class SigmaL1SmoothLoss(nn.Module):

            def forward(self, output, target):
                reg_diff = torch.abs(target - output)
                reg_loss = torch.where(torch.le(reg_diff, 1/9), 4.5 * torch.pow(reg_diff, 2), reg_diff - 1/18)
                return reg_loss.mean()

        self._ratios = [1/2,1,2]
        self._scales = [1,2**(-1/3), 2**(-2/3)]

        # The pretrained path is hardcoded to the default torch path in fastai, so we assemble the architecture first with 'pretrained'=False
        # Then we load the weights via the volume key of the d3m downloaded file
        # Then we cut the top of the model off and use it as the encoder of our RetinaNet architecture
        encoder = models.resnet50(pretrained=False)
        encoder.load_state_dict(torch.load(self._volumes['resnet50-19c8e357.pth']))
        encoder = nn.Sequential(*list(encoder.children())[:-2])

        model = RetinaNet(encoder, self._data.c, final_bias=-4)
        crit = RetinaNetFocalLoss(scales=self._scales, ratios=self._ratios)
        learn = Learner(self._data, model, loss_func=crit)

        return learn

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:

        self._training_inputs = inputs
        self._training_outputs = outputs

        inputs, _ = self._select_inputs_columns(self._training_inputs)
        outputs, _ = self._select_outputs_columns(self._training_outputs)


        self._bbox_col_name = outputs.columns[outputs.metadata.get_columns_with_semantic_type(
            'https://metadata.datadrivendiscovery.org/types/BoundingPolygon')][0]
        self._image_col_name = inputs.columns[inputs.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/FileName')][0]

        #TODO: Come back to fix this once we get a problem type that incorporates multi class
        class_col = inputs.columns[inputs.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/ClassName')]
        self._class_col_name = "class" if len(class_col) == 0 else class_col[0]


        for item in inputs.metadata.to_json_structure():
            if item.get("metadata",{}).get("location_base_uris") is not None:
                base_uri = item.get("metadata",{}).get("location_base_uris")[0]
                break

        p_parts = base_uri[7:-1].split("/")
        print("PARTS", file=sys.__stdout__)
        print(p_parts,file=sys.__stdout__)
        idx = p_parts.index("datasets")
        self._p = "/".join(p_parts[:idx + 3] + [f"{p_parts[idx + 2]}_dataset", "media"])
        print(self._p, file=sys.__stdout__)
        #self._p = "/".join(p_parts[:5] + [f"{p_parts[4]}_dataset", "media"])
        #print(self._p, file=sys.__stdout__)
        #print(inputs.head(), file=sys.__stdout__)
        #print("Input metadata", file=sys.__stdout__)
        #print(inputs.metadata.to_json_structure(), file=sys.__stdout__)
        #inputs is assuming our d3m dataframe from DatasetToDataframe
        all_data = pd.concat([inputs, outputs], axis=1)
        self._train_images, self._train_lbl_bbox =  self._get_annotations_from_d3m_dataframe(all_data)

        images, lbl_bbox = self._train_images, self._train_lbl_bbox
        #print(f"(ALL_IMAGES): {images}",file=sys.__stdout__)
        img2bbox = dict(zip(images, lbl_bbox))
        get_y_func = lambda o:img2bbox[o.name]
        def get_data(bs, size, path):

            #self._p = "/" + "/".join(self.hyperparams['dataset'].metadata.to_json_structure()[0]['metadata']['location_uris'][0].split("/")[3:-1]) + "/media"
            #print(f"FILEPATH: {self._p}",file=sys.__stdout__)
            all_p = [file for file in os.listdir(path) if file.endswith('.png') or file.endswith('.jpg') or file.endswith('.JPG')]
            #valid_imgs = [f for f in all_p if f not in images]
            src = ObjectItemList.from_folder(path)
            #print(f"(ALL_IMAGES): {[f.name for f in src.items]}",file=sys.__stdout__)
            valid_imgs = [f for f in src.items if f.name not in images]
            #print(f"len items before: {len(src.items)}", file=sys.__stdout__)
            src.items = np.array([f for f in src.items if f.name in images])
            #print(f"len items after: {len(src.items)}", file=sys.__stdout__)
            #src = ObjectItemList.from_df(df=inputs, path=p, cols="image")
            #print(valid_imgs)
            #print(len(valid_imgs))
            #src = src.split_by_files(valid_names=valid_imgs)
            src = src.split_none()
            src = src.label_from_func(get_y_func)

            src = src.transform(get_transforms(), size=size, tfm_y=True)
            #src = src.add_test(items=valid_imgs, label=None)
            return src.databunch(path=self._p, bs=bs, collate_fn=bb_pad_collate, num_workers=0)

        self._data = get_data(self.hyperparams["batch_size"], self.hyperparams["img_size"], self._p)

        return

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:

        def cthw2tlbr(boxes):
            "Convert center/size format `boxes` to top/left bottom/right corners."
            top_left = boxes[:,:2] - boxes[:,2:]/2
            bot_right = boxes[:,:2] + boxes[:,2:]/2
            return torch.cat([top_left, bot_right], 1)

        def tlbr2cthw(boxes):
            "Convert top/left bottom/right format `boxes` to center/size corners."
            center = (boxes[:,:2] + boxes[:,2:])/2
            sizes = boxes[:,2:] - boxes[:,:2]
            return torch.cat([center, sizes], 1)

        def activ_to_bbox(acts, anchors, flatten=True):
            "Extrapolate bounding boxes on anchors from the model activations."
            if flatten:
                acts.mul_(acts.new_tensor([[0.1, 0.1, 0.2, 0.2]])) #Can't remember where those scales come from, but they help regularize
                centers = anchors[...,2:] * acts[...,:2] + anchors[...,:2]
                sizes = anchors[...,2:] * torch.exp(acts[...,:2])
                return torch.cat([centers, sizes], -1)
            else: return [activ_to_bbox(act,anc) for act,anc in zip(acts, anchors)]
            return res

        def create_grid(size):
            "Create a grid of a given `size`."
            H, W = size if is_tuple(size) else (size,size)
            grid = FloatTensor(H, W, 2)
            linear_points = torch.linspace(-1+1/W, 1-1/W, W) if W > 1 else tensor([0.])
            grid[:, :, 1] = torch.ger(torch.ones(H), linear_points).expand_as(grid[:, :, 0])
            linear_points = torch.linspace(-1+1/H, 1-1/H, H) if H > 1 else tensor([0.])
            grid[:, :, 0] = torch.ger(linear_points, torch.ones(W)).expand_as(grid[:, :, 1])
            return grid.view(-1,2)

        def create_anchors(sizes, ratios, scales, flatten=True):
            "Create anchor of `sizes`, `ratios` and `scales`."
            aspects = [[[s*math.sqrt(r), s*math.sqrt(1/r)] for s in scales] for r in ratios]
            aspects = torch.tensor(aspects).view(-1,2)
            anchors = []
            for h,w in sizes:
                #4 here to have the anchors overlap.
                sized_aspects = 4 * (aspects * torch.tensor([2/h,2/w])).unsqueeze(0)
                base_grid = create_grid((h,w)).unsqueeze(1)
                n,a = base_grid.size(0),aspects.size(0)
                ancs = torch.cat([base_grid.expand(n,a,2), sized_aspects.expand(n,a,2)], 2)
                anchors.append(ancs.view(h,w,a,4))
            return torch.cat([anc.view(-1,4) for anc in anchors],0) if flatten else anchors

        def intersection(anchors, targets):
            "Compute the sizes of the intersections of `anchors` by `targets`."
            ancs, tgts = cthw2tlbr(anchors), cthw2tlbr(targets)
            a, t = ancs.size(0), tgts.size(0)
            ancs, tgts = ancs.unsqueeze(1).expand(a,t,4), tgts.unsqueeze(0).expand(a,t,4)
            top_left_i = torch.max(ancs[...,:2], tgts[...,:2])
            bot_right_i = torch.min(ancs[...,2:], tgts[...,2:])
            sizes = torch.clamp(bot_right_i - top_left_i, min=0)
            return sizes[...,0] * sizes[...,1]

        def nms(boxes, scores, thresh=self.hyperparams["nms_threshold"]):
            idx_sort = scores.argsort(descending=True)
            boxes, scores = boxes[idx_sort], scores[idx_sort]
            to_keep, indexes = [], torch.LongTensor(range_of(scores))
            while len(scores) > 0:
                to_keep.append(idx_sort[indexes[0]])
                iou_vals = IoU_values(boxes, boxes[:1]).squeeze()
                mask_keep = iou_vals < thresh
                if len(mask_keep.nonzero()) == 0: break
                boxes, scores, indexes = boxes[mask_keep], scores[mask_keep], indexes[mask_keep]
            return LongTensor(to_keep)

        def IoU_values(anchors, targets):
            "Compute the IoU values of `anchors` by `targets`."
            inter = intersection(anchors, targets)
            anc_sz, tgt_sz = anchors[:,2] * anchors[:,3], targets[:,2] * targets[:,3]
            union = anc_sz.unsqueeze(1) + tgt_sz.unsqueeze(0) - inter
            return inter/(union+1e-8)

        def process_output(output, i, dataset, detect_thresh=0.25):
            "Process `output[i]` and return the predicted bboxes above `detect_thresh`."
            clas_pred,bbox_pred,sizes = output[0][0], output[1][0], output[2]
            anchors = create_anchors(sizes, self._ratios, self._scales).to(clas_pred.device)
            bbox_pred = activ_to_bbox(bbox_pred, anchors)
            clas_pred = torch.sigmoid(clas_pred)
            detect_mask = clas_pred.max(1)[0] > detect_thresh
            bbox_pred, clas_pred = bbox_pred[detect_mask], clas_pred[detect_mask]
            bbox_pred = tlbr2cthw(torch.clamp(cthw2tlbr(bbox_pred), min=-1, max=1))

            if clas_pred.shape[0] == 0:
                return None, None, None
            scores, preds = clas_pred.max(1)

            to_keep = nms(bbox_pred, scores)
            bbox_pred, preds, scores = bbox_pred[to_keep].cpu(), preds[to_keep].cpu(), scores[to_keep].cpu()

            #print(f"PROCESSING OUTPUT: {bbox_pred}", file=sys.__stdout__)
            t_sz = torch.Tensor([*dataset[i].size])[None].float()
            bbox_pred[:,:2] = bbox_pred[:,:2] - bbox_pred[:,2:]/2
            bbox_pred[:,:2] = (bbox_pred[:,:2] + 1) * t_sz/2
            bbox_pred[:,2:] = bbox_pred[:,2:] * t_sz

            # Hack to arrange polygon / bounding box for quick fix TODO: come back here later
            #print(f"PROCESSING OUTPUT CALC: {bbox_pred}", file=sys.__stdout__)

            return bbox_pred, scores, preds

        def center_size_to_coords(bbox):
            #top left
            y1 = round(float(bbox[0] - bbox[2]/2),2)
            y1 = int(bbox[0] - bbox[2]/2)
            x1 = round(float(bbox[1] - bbox[3]/2),2)
            x1 = int(bbox[1] - bbox[3]/2)
            #top right
            y2 = round(float(bbox[0] - bbox[2]/2),2)
            y2 = int(bbox[0] - bbox[2]/2)
            x2 = round(float(bbox[1] + bbox[3]/2),2)
            x2 = int(bbox[1] + bbox[3]/2)
            #bottom right
            y3 = round(float(bbox[0] + bbox[2]/2),2)
            y3 = int(bbox[0] + bbox[2]/2)
            x3 = round(float(bbox[1] + bbox[3]/2),2)
            x3 = int(bbox[1] + bbox[3]/2)
            #bottom left
            y4 = round(float(bbox[0] + bbox[2]/2),2)
            y4 = int(bbox[0] + bbox[2]/2)
            x4 = round(float(bbox[1] - bbox[3]/2),2)
            x4 = int(bbox[1] - bbox[3]/2)

            return f"{x1},{y1},{x4},{y4},{x3},{y3},{x2},{y2}"


        # Forward pass through our model of input images and calculate our bbox's, scores, and preds and then
        # go through consolidation via nms and score confidence
        test_dataset = ImageList.from_folder(self._p)
        produce_imgs = inputs[self._image_col_name].unique().tolist()
        d3m_indices = inputs["d3mIndex"].unique().tolist()
        #print(f"(PRODUCE): {produce_imgs[:10]}",file=sys.__stdout__)
        #print(f"(PRODUCE) len input images: {len(produce_imgs)}", file=sys.__stdout__)
        #print(f"(PRODUCE) len items before: {len(test_dataset.items)}", file=sys.__stdout__)
        test_dataset.items = np.array([f for f in test_dataset.items if f.name in produce_imgs])
        #print(f"(PRODUCE) len items after: {len(test_dataset.items)}", file=sys.__stdout__)
        # output_df = DataFrame(columns = ["d3mIndex",self._image_col_name,self._class_col_name,"confidence",self._bbox_col_name])
        #
        # output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 0), 'https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey')
        # output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 0), 'https://metadata.datadrivendiscovery.org/types/Integer')
        #
        # output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/Text')
        #
        # #output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')
        #
        # output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 3), 'https://metadata.datadrivendiscovery.org/types/Confidence')
        #
        # output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 4), 'https://metadata.datadrivendiscovery.org/types/BoundingPolygon')
        # output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 4), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        output_df = DataFrame(columns = ["d3mIndex", self._bbox_col_name, "confidence"])

        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 0),
                                                                  'https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey')
        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 0),
                                                                  'https://metadata.datadrivendiscovery.org/types/Integer')

        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 1),
                                                                  'https://metadata.datadrivendiscovery.org/types/BoundingPolygon')
        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 1),
                                                                  'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 2),
                                                                  'https://metadata.datadrivendiscovery.org/types/Confidence')



        print(test_dataset)
        print(f"Predicting: {len(test_dataset)} images")

        with torch.random.fork_rng() as sess:
            torch.manual_seed(self.random_seed)

            for i in range(len(test_dataset.items)):
                print(f"Prediction {i + 1} of {len(test_dataset.items)}",file=sys.__stdout__)
                #print(test_dataset[i])
                inp = test_dataset[i].data[None]
                fastai.torch_core.defaults.device = torch.device('cpu')
                # if self.hyperparams['use_gpu']:
                #     inp = inp.cuda()
                self._learn.model = self._learn.model.cpu()
                outputs = self._learn.model(inp)
                #print(outputs)
                bbox_pred, scores, preds = process_output(outputs, i, test_dataset, detect_thresh=self.hyperparams["detect_threshold"])
                #print("bbox predsssss: ",file=sys.__stdout__)
                #print(bbox_pred,file=sys.__stdout__)
                sub = inputs[inputs[self._image_col_name] == test_dataset.items[i].name]
                d3mIndex = sub.iloc[0]["d3mIndex"]
                image = test_dataset.items[i].name

                if bbox_pred is not None:
                    for j in range(len(scores)):
                        pred_class = self._learn.data.classes[preds[j]+1]
                        pred_confidence = float(scores[j].data)
                        pred_bbox = center_size_to_coords(bbox_pred[j])

                        #data = {"d3mIndex":d3mIndex, self._image_col_name:image, self._class_col_name:pred_class, "confidence":pred_confidence, self._bbox_col_name:pred_bbox}
                        data = {"d3mIndex":d3mIndex, "confidence":pred_confidence, self._bbox_col_name:pred_bbox}
                        #print(data)
                        output_df = output_df.append(pd.Series(data), ignore_index=True)
                else:
                    #data = {"d3mIndex":d3mIndex, self._image_col_name:image, self._class_col_name:"n/a", "confidence":0, self._bbox_col_name:"0,0,0,0,0,0,0,0"}
                    data = {"d3mIndex": d3mIndex, self._bbox_col_name: "0,0,0,0,0,0,0,0", "confidence": 0}
                    output_df = output_df.append(pd.Series(data), ignore_index=True)

            missing_d3mIndices = list(set(d3m_indices).difference(set(output_df["d3mIndex"].unique().tolist())))
            for idx in missing_d3mIndices:
                #data = {"d3mIndex":idx, self._image_col_name:"n/a", self._class_col_name:"n/a", "confidence":0, self._bbox_col_name:"0,0,0,0,0,0,0,0"}
                data = {"d3mIndex": idx, self._bbox_col_name: "0,0,0,0,0,0,0,0", "confidence": 0}
                output_df = output_df.append(pd.Series(data), ignore_index=True)

            #print("OUTPUT: ",file=sys.__stdout__)
            #print(output_df,file=sys.__stdout__)
            #print(output_df.metadata, file=sys.__stdout__)
            #print(output_df.metadata.to_json_structure(), file=sys.__stdout__)

            #cls._can_use_inputs_column(inputs_metadata, column_index)

            # outputs = base_utils.combine_columns(inputs, columns_to_use, output_columns,
            #                                      return_result=self.hyperparams['return_result'],
            #                                      add_index_columns=self.hyperparams['add_index_columns'])

        return CallResult(output_df)

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        #print(self._training_inputs, file=sys.__stdout__)

        # inputs, _ = self._select_inputs_columns(self._training_inputs)
        # outputs, _ = self._select_outputs_columns(self._training_outputs)
        # print('IN FITTING ', file=sys.__stdout__)
        #
        #
        #
        # print(inputs, file=sys.__stdout__)
        # print(outputs, file=sys.__stdout__)
        #
        #
        #
        # print("extracted input bbox", file=sys.__stdout__)
        # print(outputs.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/BoundingPolygon'), file=sys.__stdout__)
        # print(outputs.columns[outputs.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/BoundingPolygon')][0], file=sys.__stdout__)

        def retina_net_split(model):
            groups = [list(model.encoder.children())[:6], list(model.encoder.children())[6:]]
            return groups + [list(model.children())[1:]]

        with torch.random.fork_rng() as sess:
            torch.manual_seed(self.random_seed)

            self._learn = self._create_learner()
            self._learn.silent = True
            self._learn = self._learn.split(retina_net_split)
            self._learn.freeze()

            if iterations is None:
                iterations = 5
            pre = math.ceil(iterations / 4)
            post = iterations - pre

            self._learn.fit_one_cycle(pre, self.hyperparams["learning_rate"])
            self._learn.unfreeze()
            self._learn.fit_one_cycle(post, self.hyperparams["learning_rate"])

        return CallResult(None)

    def continue_fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:

        with torch.random.fork_rng() as sess:
            torch.manual_seed(self.random_seed)

            if iterations is None:
                iterations = 1
            self._learn.fit_one_cycle(iterations, self.hyperparams["learning_rate"])

        return CallResult(None)


    def get_params(self) -> Params:
        pass

    def set_params(self, *, params: Params) -> None:
        pass

    """
    Helper methods (Will be moved to SupervisedLearner Base class eventually

    """

    @classmethod
    def _can_use_inputs_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))
        #print(column_metadata, file=sys.__stdout__)
        accepted_structural_types = (str)
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        return 'https://metadata.datadrivendiscovery.org/types/Attribute' in column_metadata.get('semantic_types', [])

    @classmethod
    def _get_inputs_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_inputs_column(inputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(inputs_metadata,
                                                                      hyperparams['use_inputs_columns'],
                                                                      hyperparams['exclude_inputs_columns'],
                                                                      can_use_column)

        if not columns_to_use:
            raise ValueError("No inputs columns.")

        if hyperparams['use_inputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified inputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    @classmethod
    def _can_use_outputs_column(cls, outputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = outputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        return 'https://metadata.datadrivendiscovery.org/types/SuggestedTarget' in column_metadata.get('semantic_types',
                                                                                                       [])

    @classmethod
    def _get_outputs_columns(cls, outputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_outputs_column(outputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(outputs_metadata,
                                                                      hyperparams['use_outputs_columns'],
                                                                      hyperparams['exclude_outputs_columns'],
                                                                      can_use_column)

        if not columns_to_use:
            raise ValueError("No outputs columns.")

        if hyperparams['use_outputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified outputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    def _select_inputs_columns(self, inputs: Inputs) -> Tuple[Inputs, List[int]]:
        columns_to_use = self._get_inputs_columns(inputs.metadata, self.hyperparams)

        return utils.select_columns(inputs, columns_to_use, source=self), columns_to_use

    def _select_outputs_columns(self, outputs: Outputs) -> Tuple[Outputs, List[int]]:
        columns_to_use = self._get_outputs_columns(outputs.metadata, self.hyperparams)

        return utils.select_columns(outputs, columns_to_use, source=self), columns_to_use

    @classmethod
    def _add_target_semantic_types(cls, metadata: metadata_base.DataMetadata,
                                   source: typing.Any, target_names: List = None, ) -> metadata_base.DataMetadata:
        for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/Target',
                                                  source=source)
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                                                  source=source)
            if target_names:
                metadata = metadata.update((metadata_base.ALL_ELEMENTS, column_index), {
                    'name': target_names[column_index],
                }, source=source)
        return metadata

    def _wrap_predictions(self, predictions: np.ndarray) -> Outputs:
        outputs = d3m_dataframe(predictions, generate_metadata=False)
        outputs.metadata = self._update_predictions_metadata(outputs, self._target_columns_metadata)
        outputs.columns = self._target_columns_names
        return outputs

    @classmethod
    def _update_predictions_metadata(cls, outputs: Optional[Outputs],
                                     target_columns_metadata: List[OrderedDict]) -> metadata_base.DataMetadata:
        outputs_metadata = metadata_base.DataMetadata()
        if outputs is not None:
            outputs_metadata = outputs_metadata.generate(outputs)

        for column_index, column_metadata in enumerate(target_columns_metadata):
            outputs_metadata = outputs_metadata.update_column(column_index, column_metadata)

        return outputs_metadata

    def _store_columns_metadata_and_names(self, inputs: Inputs, outputs: Outputs) -> None:
        self._attribute_columns_names = list(inputs.columns)
        self._target_columns_metadata = self._get_target_columns_metadata(outputs.metadata)
        self._target_columns_names = list(outputs.columns)

    @classmethod
    def _get_target_columns_metadata(cls, outputs_metadata: metadata_base.DataMetadata) -> List[OrderedDict]:
        outputs_length = outputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        target_columns_metadata: List[OrderedDict] = []
        for column_index in range(outputs_length):
            column_metadata = OrderedDict(outputs_metadata.query_column(column_index))

            # Update semantic types and prepare it for predicted targets.
            semantic_types = list(column_metadata.get('semantic_types', []))
            if 'https://metadata.datadrivendiscovery.org/types/PredictedTarget' not in semantic_types:
                semantic_types.append('https://metadata.datadrivendiscovery.org/types/PredictedTarget')
            semantic_types = [semantic_type for semantic_type in semantic_types if
                              semantic_type != 'https://metadata.datadrivendiscovery.org/types/TrueTarget']
            column_metadata['semantic_types'] = semantic_types

            target_columns_metadata.append(column_metadata)

        return target_columns_metadata
