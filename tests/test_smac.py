from jpl_primitives.optimizers import smac_optimizer
import numpy as np

from d3m.metadata import base as metadata_base
from d3m import container
from d3m.exceptions import PrimitiveNotFittedError
from pandas.util.testing import assert_frame_equal
import pandas as pd
from common_primitives import dataset_to_dataframe, extract_columns_semantic_types, random_forest, utils, column_parser
import common_primitives.utils as common_utils
import os
import unittest
import numpy as np
import pickle

# Common random state
rng = np.random.RandomState(0)

from common_primitives import dataset_to_dataframe, column_parser
import os

dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "tests/data/datasets/iris_dataset_1/datasetDoc.json"))
dataset = container.Dataset.load(dataset_uri="file://{}".format(dataset_doc_path))
hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())
call_metadata = primitive.produce(inputs=dataset)

dataframe = call_metadata.value
column_parser_htperparams = column_parser.Hyperparams.defaults()
column_parser_primitive = column_parser.ColumnParserPrimitive(hyperparams=column_parser_htperparams)
parsed_dataframe = column_parser_primitive.produce(inputs=dataframe).value
parsed_dataframe.metadata = parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/Target')
parsed_dataframe.metadata = parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
parsed_dataframe.metadata = parsed_dataframe.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/Attribute')
parsed_dataframe.metadata = parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/CategoricalData')

train_set = targets = parsed_dataframe

class TestSMACOptimizerPrimitive(unittest.TestCase):
    def fit(self, hyperparams, train_set, targets):
        clf = smac_optimizer.SMACOptimizerPrimitive(hyperparams=hyperparams)
        clf.set_training_data(inputs=train_set, outputs=targets)
        clf.fit()
        output = clf.produce(inputs=train_set)
        clf.produce_optimal_hyperparameters()
        print(output.value)

    def test_with_semantic_types(self):
        hyperparams = smac_optimizer.Hyperparams.defaults().replace({"use_semantic_types": True, "max_evals":12})
        self.fit(hyperparams, train_set, targets)

    def test_without_semantic_types(self):
        hyperparams = smac_optimizer.Hyperparams.defaults().replace({"max_evals":12})
        self.fit(hyperparams, parsed_dataframe.select_columns([1, 2, 3, 4]), parsed_dataframe.select_columns([5]))

    def test_with_return_result(self):
        return_result = ['append', 'replace', 'new']
        for value in return_result:
            hyperparams = smac_optimizer.Hyperparams.defaults().replace(
                {"return_result": value, "max_evals":12, "use_semantic_types": True})
            self.fit(hyperparams, train_set, targets)


if __name__ == '__main__':
    unittest.main()