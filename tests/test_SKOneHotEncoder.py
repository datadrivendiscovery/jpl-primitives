import unittest
import numpy as np
import pickle
from typing import NamedTuple

from sklearn import datasets
from sklearn.utils import shuffle
import scipy.sparse

#from autogenerate.d3m_sklearn_wrap.sklearn_wrap import SKOneHotEncoder
from jpl_primitives.tpot import SKOneHotEncoder
from tests.params_check import params_check
from d3m.metadata import base as metadata_base
from d3m import container
from d3m.exceptions import PrimitiveNotFittedError
from pandas.util.testing import assert_frame_equal
import pandas as pd
from common_primitives import dataset_to_dataframe, extract_columns_semantic_types, random_forest, utils, column_parser
import common_primitives.utils as common_utils
import os

# Common random state
rng = np.random.RandomState(0)

# Custom dataset
# X = [['Male', 1], ['Female', 3], ['Female', 2]]
# train_set = container.DataFrame(X, generate_metadata=True)
# column_parser_htperparams = column_parser.Hyperparams.defaults()
# column_parser_primitive = column_parser.ColumnParserPrimitive(
#     hyperparams=column_parser_htperparams)
# parsed_dataframe = column_parser_primitive.produce(inputs=train_set).value
# for idx in range(len(parsed_dataframe.columns)):
#     parsed_dataframe.metadata = parsed_dataframe.metadata.add_semantic_type(
#         (metadata_base.ALL_ELEMENTS, idx), 'https://metadata.datadrivendiscovery.org/types/Attribute')
#     parsed_dataframe.metadata = parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, idx),
#                                                                             'https://metadata.datadrivendiscovery.org/types/CategoricalData')

# targets = train_set = parsed_dataframe

# Custom dataset
X = {"col1": [7, 2, 3,3,3,3,3,3,3,3,3,3,3,3,3,3], "col2": [1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16], "col3": [1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16]}
train_set = container.DataFrame(X, generate_metadata=True)
column_parser_htperparams = column_parser.Hyperparams.defaults()
column_parser_primitive = column_parser.ColumnParserPrimitive(
    hyperparams=column_parser_htperparams)
parsed_dataframe = column_parser_primitive.produce(inputs=train_set).value
for idx in range(len(parsed_dataframe.columns)):
    parsed_dataframe.metadata = parsed_dataframe.metadata.add_semantic_type(
        (metadata_base.ALL_ELEMENTS, idx), 'https://metadata.datadrivendiscovery.org/types/Attribute')

targets = train_set = parsed_dataframe
inputs_2 = [[0, 1, 1]]


class TestSKOneHotEncoder(unittest.TestCase):
    def fit(self, hyperparams, train_set=train_set):
        clf = SKOneHotEncoder.SKOneHotEncoder(hyperparams=hyperparams)

        clf.set_training_data(inputs=train_set)
        assert len(clf._training_indices) > 0

        # Testing if pickling is possible before fitting
        model = pickle.dumps(clf)
        pickle.loads(model)

        self.assertRaises(PrimitiveNotFittedError,
                          clf.produce, inputs=train_set)
        clf.fit()
        first_output = clf.produce(inputs=train_set)
        
        model = pickle.dumps(clf)
        model2 = pickle.loads(model)

        new_output = model2.produce(inputs=train_set)
        
        if hyperparams['return_result'] == 'new' or hyperparams['return_result'] == 'replace':
            # check that attribute semantic type exists
            selected_input_columns = clf.get_params()["training_indices_"]
            for i in range(len(selected_input_columns)):
                input_semantic_types = train_set.metadata.query_column(
                    selected_input_columns[i]).get("semantic_types")
                for semantic_type in input_semantic_types:
                    if hyperparams['return_result'] == 'new':
                        assert first_output.value.metadata.has_semantic_type((metadata_base.ALL_ELEMENTS, i),
                                                                             semantic_type) == True
                    else:
                        assert first_output.value.metadata.has_semantic_type((metadata_base.ALL_ELEMENTS, selected_input_columns[i]),
                                                                             semantic_type) == True

        # We want to test the running of the code without errors and not the correctness of it
        # since that is assumed to be tested by sklearn

    def test_with_semantic_types(self):
        hyperparams = SKOneHotEncoder.Hyperparams.defaults().replace(
            {"use_semantic_types": True})
        self.fit(hyperparams, train_set)

    def test_without_semantic_types(self):
        hyperparams = SKOneHotEncoder.Hyperparams.defaults()
        self.fit(hyperparams, train_set)

    def test_with_return_result(self):
        return_result = ['append', 'replace', 'new']
        for value in return_result:
            hyperparams = SKOneHotEncoder.Hyperparams.defaults().replace(
                {"return_result": value, "use_semantic_types": True})
            self.fit(hyperparams, train_set)


if __name__ == '__main__':
    unittest.main()
