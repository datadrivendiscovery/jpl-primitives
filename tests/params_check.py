"""
File for testing if the right hyper-parameters and parameters are present in the primitive.

`params_check` detects if there's hyper-parameter or parameter that shouldn't be there, and if there's
hyper-parameter or parameter missing from the primitive.

It does that by comparing parameters from __getstate__ function (sklearn) with get_params function
merged with hyper-parameters (wrapper primitive).

It also checks if parameters belong to the right group. If parameter should be hyper-parameter (or vise versa),
an error is thrown. That is done by comparing our hyper-parameters with parameters retrieved from the JSON
of the primitive.

Test should be present in all UNIT tests except for transformers.
"""

import json
import os
import typing
from importlib import import_module
from inspect import getfullargspec

from sklearn.svm import SVR

current_dir = os.path.abspath(os.path.join(
    os.path.realpath(__file__), os.pardir))
primitive_jsons_path = os.path.join(current_dir, 'resources/primitive-jsons')
overlays_path = os.path.join(current_dir, 'resources')

# Parameters in sklearn that can't be in wrapper.
only_in_sklearn = {
    '_sklearn_version',
    'kernel_params',  # is realized differently (by other hyper-parameters)

    # standard constructor arguments
    'random_state',
    'verbose',

    # handled by the primitive itself
    'copy',
    'copy_X',
    'copy_x',

    # future hyper-parameters
    'compute_score',
    'estimator',
    'class_prior',
    'priors',
    'preprocessor',
    'pooling_func',
    'tokenizer',
    'init',
    'memory',
    'dtype',
    'metric_params',
    'separator',  # in DictVectorizer
    'input',  # in CountVectorizer, TfidfVectorizer
    'encoding',  # in CountVectorizer, TfidfVectorizer
    'decode_error',  # in CountVectorizer, TfidfVectorizer
    'vocabulary',  # in CountVectorizer, TfidfVectorizer

}

# Hyper-parameters that are specific to wrapper.
only_in_wrapper_hyperparams = {
    'add_index_columns',
    'exclude_columns',
    'exclude_input_columns',
    'exclude_output_columns',
    'return_result',
    'use_columns',
    'use_input_columns',
    'use_output_columns',
    'use_semantic_types',
    'error_on_no_input'
}
# Parameters that are specific to wrapper.
only_in_wrapper_params = {
    'target_names_',
    'training_indices_',
    'target_column_indices_',
    'target_columns_metadata_',
    'input_column_names'
}

# Deprecated hyper-parameters, they can't be in wrapper primitive.
deprecated_hyperparams = {
    'min_impurity_split',
    'n_iter',
}

# Exceptions, errors won't be raised on these.
ignore = {
    # you cannot define kernel by passing a string here, so it doesn't make sense as hyper-parameter
    'GaussianProcessRegressor': {'kernel'},
    # it's parameter in some other primitives, that's why it isn't in 'only_in_sklearn'
    'BaggingClassifier': {'base_estimator'},
    'TruncatedSVD': {'n_iter'},  # it isn't deprecated here
    'ARDRegression': {'n_iter'},  # it isn't deprecated here
    # loss is hyperparameter for classifier not regressor
    'MLPRegressor': {'loss'},
    'MLPClassifier': {'loss'},  # loss is not in the init for MLPCLassifier
}


def write_params(name: str, wicked_params: set, problem: str, *, source_class: typing.ClassVar = None, all_params: set = None) -> str:
    """
    Transforms parameters ``wicked_params`` into more human readable form.

    If ``source_class`` is present, each parameter is given its type.

    If ``all_params`` is present, ``wicked_params`` are removed from ``all_params``. That's
    used if you want that some parameters are reported only once.

    Parameters
    ----------
    name : str
        Name of the primitive we are testing.
    wicked_params : set
        Parameters that are to be merged into one string.
    problem : str
        Description of why parameter is reported and what's possibly a solution.
    source_class : typing.ClassVar
        Class that ``wicked_params`` belong to.
    all_params : set
        Set, from which ``wicked_params`` were selected from.

    Returns
    -------
        String, concatenation of all parameters.
    """

    if all_params is not None:
        all_params.difference_update(wicked_params)

    if name in ignore:
        wicked_params = wicked_params.difference(ignore[name])

    if source_class is None:
        return ''.join(["  - `{param}` {problem}.\n".format(param=param, problem=problem) for param in sorted(wicked_params)])
    return ''.join(["  - `{param}` {type} {problem}.\n".format(param=param, type=type(getattr(source_class, param)), problem=problem) for param in sorted(wicked_params)])


def check(errors: str, name: str) -> None:
    """
    Throws an error if ``errors`` has length greater than 0.

    Parameters
    ----------
    errors : str
        Errors in a string.
    name : str
        Name of the wrapper primitive.
    """

    assert len(errors) == 0, "\n\nError(s) in SK{name} primitive:\n" \
                             "{errors}" \
                             "If you think this isn't the case, you have two options:\n" \
                             "  1) Manipulate with sets at the beginning of the `params_check.py` to apply a general rule for all primitives.\n" \
                             "  2) Add it to `ignore` in `params_check.py`." \
        .format(name=name, errors=errors)


def params_check(wrapper, input_data=None, output_data=None):
    """
    Collects all errors and reports them once, so user can fix them in one step.

    Parameters
    ----------
    wrapper : class
        Wrapper primitive.
    input_data
        Input data passed to the fit method.
    output_data
        Output data passed to the fit method.
    """

    # Placeholder for errors.
    errors = ''

    # Getting instance of the wrapper primitive.
    hyperparams = wrapper.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults(
    ).replace({"use_semantic_types": True})
    wrapper = wrapper(hyperparams=hyperparams)

    # Fitting.
    kwargs = {}
    if input_data is not None:
        kwargs['inputs'] = input_data
    if output_data is not None:
        kwargs['outputs'] = output_data
    wrapper.set_training_data(**kwargs)
    wrapper.fit()

    # Getting instance of the sklearn class.
    module = wrapper.metadata.query()['name']
    name = module[module.rfind('.') + 1:]
    sklearn = getattr(import_module(module[:module.rfind('.')]), name)

    # Setting all flags to True, so '__getstate__' function contains as many parameters as possible.
    kwargs = {} if name != 'RFE' else {'estimator': SVR(kernel='linear')}
    args = getfullargspec(sklearn)
    for i, arg in enumerate(args[3] if args[3] is not None else []):
        if arg is False and args[0][i + 1] not in only_in_sklearn.union({'warm_start'}):
            kwargs[args[0][i + 1]] = True

    # If subsample is 1, '__getstate__' function doesn't have some parameters.
    if 'subsample' in args[0]:
        kwargs['subsample'] = .9

    sklearn = sklearn(**kwargs)

    # Fitting.
    args = []
    if input_data is not None:
        args.append(wrapper._training_inputs)
    if output_data is not None:
        args.append(wrapper._training_outputs)
    # Vectorizers need to have `list` as an input, not DataSet.
    # TODO: Remove next two lines after https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/108 is fixed.
    if name.endswith('Vectorizer'):
        args = [input_data[0]]
    sklearn.fit(*args)

    # Getting parameters.
    wrapper_params = set(wrapper.get_params().keys() if wrapper.get_params(
    ) is not None else []).difference(only_in_wrapper_params)
    wrapper_hyperparams = set(hyperparams.keys()).difference(
        only_in_wrapper_hyperparams)
    sklearn_all = set(sklearn.__getstate__().keys()).difference(
        only_in_sklearn).difference(deprecated_hyperparams)
    sklearn_hyperparams = {}
    for file_name in os.listdir(primitive_jsons_path):
        with open(os.path.join(primitive_jsons_path, file_name)) as file:
            primitive = json.load(file)
            if primitive['name'].endswith(name[2:]):
                sklearn_hyperparams = {param['name']
                                       for param in primitive.get('parameters', [])}
                break
    sklearn_hyperparams.difference_update(
        only_in_sklearn.union(deprecated_hyperparams))

    # Adding sub-hyper-parameters from 'Choice' hyper-parameters
    matching_overlays = 0
    for file_name in os.listdir(overlays_path):
        if file_name.endswith('.json'):
            with open(os.path.join(overlays_path, file_name)) as file:
                overlay = json.load(file)
                if name in overlay:
                    matching_overlays += 1
                    primitive = overlay[name]
                    for hyperparam in primitive['Hyperparams']:
                        if hyperparam['type'] == 'Choice':
                            if 'parameters_placeholder' in hyperparam:
                                wrapper_hyperparams.add(
                                    hyperparam['parameters_placeholder'])
                            else:
                                wrapper_hyperparams.update(
                                    set(hyperparam['hyperparams'].keys()))
    assert matching_overlays == 1, "Primitive should be in exactly one overlay."

    # Ensuring that we don't have `only_in_*` parameters anywhere.
    errors += write_params(name, sklearn_all.intersection(only_in_wrapper_params),
                           "is in sklearn and in `only_in_wrapper_params`, which shouldn't happen")
    errors += write_params(name, sklearn_all.intersection(only_in_wrapper_hyperparams),
                           "is in sklearn and in `only_in_wrapper_hyperparams`, which shouldn't happen")
    errors += write_params(name, wrapper_params.intersection(only_in_sklearn),
                           "shouldn't be in `Params` (it's in `only_in_sklearn`)")
    errors += write_params(name, wrapper_hyperparams.intersection(only_in_sklearn),
                           "shouldn't be hyper-parameter (it's in `only_in_sklearn`)")

    # We want that that above errors are fixed before proceeding, otherwise we might produce misleading errors.
    check(errors, name)

    # Looking for params that are properties.
    properties = {param for param in wrapper_params.union(wrapper_hyperparams) if hasattr(
        type(sklearn), param) and isinstance(getattr(type(sklearn), param), property)}
    errors += write_params(name, wrapper_params.intersection(properties),
                           "shouldn't be in `Params` (it's property)",
                           all_params=wrapper_params)

    # Looking for deprecated hyperparams.
    errors += write_params(name, wrapper_hyperparams.intersection(deprecated_hyperparams),
                           "shouldn't be hyper-parameter (it's deprecated hyper-parameter)",
                           all_params=wrapper_hyperparams)

    # Looking for params that are actually hyperparams.
    errors += write_params(name, wrapper_params.intersection(wrapper_hyperparams.union(sklearn_hyperparams)),
                           "shouldn't be in `Params` (it's hyper-parameter)",
                           all_params=wrapper_params)

    # It's okay if hyper-parameter is property.
    wrapper_hyperparams.difference_update(properties)

    # Looking for parameters and hyper-parameters that should be in wrapper primitive.
    missing = sklearn_all.difference(
        wrapper_params).difference(wrapper_hyperparams)
    errors += write_params(name, missing.intersection(sklearn_hyperparams),
                           "should be hyper-parameter (it's in __getstate__ function of sklearn class)",
                           all_params=missing,
                           source_class=sklearn)
    errors += write_params(name, missing,
                           "should be added to the primitive (it's either missing parameter or missing hyper-parameter)",
                           source_class=sklearn)

    # Looking for parameters and hyper-parameters that shouldn't be in wrapper primitive.
    errors += write_params(name, wrapper_params.difference(sklearn_all),
                           "shouldn't be in `Params` (it isn't in __getstate__ function of sklearn class)")
    errors += write_params(name, wrapper_hyperparams.difference(sklearn_all),
                           "shouldn't be hyper-parameter (it isn't in __getstate__ function of sklearn class)")

    check(errors, name)
