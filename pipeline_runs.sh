#!/bin/bash

# JPL Primitives example running of pipelines via the CLI interface

if [[ $1 == "keras" ]]; then
    # Keras Wrap Pipeline

    python3 -m d3m runtime fit-score \
        --pipeline /d3m/jpl-primitives/pipelines/learner.model.KerasWrap/pipelines/32418dc0-a64d-4cc6-af3f-671a828a22e3.yml \
        -r /d3m/datasets/training_datasets/LL1/124_120_mnist/124_120_mnist_problem/problemDoc.json \
        --input /d3m/datasets/training_datasets/LL1/124_120_mnist/124_120_mnist_dataset/datasetDoc.json \
        --test-input /d3m/datasets/training_datasets/LL1/124_120_mnist/124_120_mnist_dataset/datasetDoc.json \
        --score-input /d3m/datasets/training_datasets/LL1/124_120_mnist/124_120_mnist_dataset/datasetDoc.json \
        --output-run /d3m/jpl-primitives/pipelines/learner.model.KerasWrap/pipeline_runs/pipeline_run.yml

elif [[ $1 == "retina_net" ]]; then
    # Retina_Net Pipeline
    # For training object detector from scratch (probably not feasible becasause of d3m limitations)

    python3 -m d3m runtime fit-score \
        --pipeline /d3m/jpl-primitives/pipelines/object_detection.retina_net.JPLPrimitives/pipelines/2aa021cf-4dfc-4d46-82bc-738017555bd2.yml \
        -r /d3m/datasets/seed_datasets_current/LL1_penn_fudan_pedestrian/LL1_penn_fudan_pedestrian_problem/problemDoc.json \
        --input /d3m/datasets/seed_datasets_current/LL1_penn_fudan_pedestrian/TRAIN/dataset_TRAIN/datasetDoc.json \
        --test-input /d3m/datasets/seed_datasets_current/LL1_penn_fudan_pedestrian/TEST/dataset_TEST/datasetDoc.json \
        --score-input /d3m/datasets/seed_datasets_current/LL1_penn_fudan_pedestrian/SCORE/dataset_SCORE/datasetDoc.json \
        --output-run /d3m/jpl-primitives/pipelines/object_detection.retina_net.JPLPrimitives/pipeline_runs/pipeline_run.yml

elif [[ $1 == "faster_rcnn" ]]; then
    # Faster RCNN Pipeline
    # For no training but using inference out of the box -> contrast to Retina_Net implementation

    python3 -m d3m runtime \
        --volumes /d3m/jpl-primitives \
        -d /d3m/datasets/seed_datasets_current fit-score \
        -n /d3m/jpl-primitives/scoring.yml \
        -m /d3m/jpl-primitives/pipelines/object_detection.faster_rcnn.JPLPrimitives/pipelines/0f94e783-46b3-49f1-b229-abc7ec18e63a.meta \
        -p /d3m/jpl-primitives/pipelines/object_detection.faster_rcnn.JPLPrimitives/pipelines/0f94e783-46b3-49f1-b229-abc7ec18e63a.yml 
        
else
    echo "Your input does not match a pipeline"
fi

echo "Finished"